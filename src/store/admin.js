import Vue from 'vue';
import Vuex from 'vuex';
import Doctors from './modules/doctors/index';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    doctorStore: Doctors
  },
  strict: debug
});
