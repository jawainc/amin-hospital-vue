/**
 ******************************
 Doctors List
 ******************************
 [
  {
    id: (id of doctor)
    name: (name of doctor)
    fee: (doctor fee)
    percentage: (percentage of doctor)
  }
 ]
 */

import _ from 'lodash';

export default {
  namespaced: true,
  state: {
    selectedDoctors: []
  },
  getters: {
    /**
     * return doctors list
     * @param state
     * @returns {Array}
     */
    getSelectedDoctors: (state) => {
      return state.selectedDoctors;
    },
    /**
     * get doctor with id
     * @param state
     */
    getDoctor: (state) => (doctorID) => {
      return _.find(state.selectedDoctors, _.matches({id: doctorID}));
    },
    /**
     * use this method to get copy of doctors, avoiding reference
     * return doctors data
     * @param state
     * @returns {Array}
     */
    getDoctorsDataCopy: (state) => {
      return _.cloneDeep(state.selectedDoctors);
    }
  },
  mutations: {
    /**
     * sets selectedDoctors via deep copy
     * @param state
     * @param val
     */
    setSelectedDoctors (state, val) {
      if (val.length > 0) {
        state.selectedDoctors = _.cloneDeep(val);
      }
    },
    /**
     * change percentage of doctor
     * @param state
     * @param val
     */
    changePercentage (state, payload) {
      let d = _.find(state.selectedDoctors, _.matches({id: payload.doctorID}));
      if (d) {
        let number = parseInt(payload.val);
        if (_.isFinite(number)) {
          d.percentage = number;
        } else {
          d.percentage = 0;
        }
        d.totalPercentage = d.percentage;
        d.assistants.forEach(function (item) {
          d.totalPercentage += item.percentage;
        });
        d.percentageError = (d.totalPercentage < 0 || d.totalPercentage > 100);
      }
    },
    /**
     * change fee of doctor
     * @param state
     * @param val
     */
    changeFee (state, payload) {
      let d = _.find(state.selectedDoctors, _.matches({id: payload.doctorID}));
      if (d) {
        let number = parseInt(payload.val);
        if (_.isFinite(number) && number > -1) {
          d.fee = number;
          d.feeError = false;
        } else {
          d.fee = payload.val;
          d.feeError = true;
        }
      }
    },
    /**
     * change assistant percentage
     * @param state
     * @param payload
     */
    changeAssistantPercentage (state, payload) {
      let d = _.find(state.selectedDoctors, _.matches({id: payload.doctorID}));
      if (d) {
        let s = _.find(d.assistants, _.matches({id: payload.assistantID}));
        if (s) {
          let number = parseInt(payload.val);
          if (_.isFinite(number)) {
            s.percentage = number;
          } else {
            s.percentage = 0;
          }
          d.totalPercentage = d.percentage;
          d.assistants.forEach(function (item) {
            d.totalPercentage += item.percentage;
          });
          d.percentageError = (d.totalPercentage < 0 || d.totalPercentage > 100);
        }
      }
    },
    /**
     * add assistant to doctor
     * @param state
     * @param payload
     */
    addAssistants (state, payload) {
      let d = _.find(state.selectedDoctors, _.matches({id: payload.doctorID}));
      if (d) {
        d.assistants = _.cloneDeep(payload.assistants);
        d.totalPercentage = d.percentage;
        d.percentageError = (d.totalPercentage < 0 || d.totalPercentage > 100);
      }
    },
    /**
     * reset doctors list
     * @param state
     */
    reset (state) {
      state.selectedDoctors = [];
    },
    /**
     * removes doctor from list at index
     * @param state
     * @param index
     */
    removeDoctor (state, index) {
      state.selectedDoctors.splice(index, 1);
    },
    /**
     * remove assistant
     * @param state
     * @param payload
     */
    removeAssistant (state, payload) {
      let doc = _.find(state.selectedDoctors, _.matches({id: payload.doctorID}));
      if (doc) {
        let assistantIndex = _.findIndex(doc.assistants, {id: payload.assistantID});
        if (assistantIndex > -1) {
          doc.assistants.splice(assistantIndex, 1);
          doc.totalPercentage = doc.percentage;
          doc.assistants.forEach(function (item) {
            doc.totalPercentage += item.percentage;
          });
          doc.percentageError = (doc.totalPercentage < 0 || doc.totalPercentage > 100);
        }
      }
    }
  }
};
