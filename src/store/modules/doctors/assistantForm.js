/**
 -----------------------------------------------------
 Doctors Form
 -----------------------------------------------------
 [
  {
    doctor_id: (id of doctor)
    rows: [
      { <-- ROW
        cells: [
          { <-- CELL
            gr: (cell length)
            text_align: (left, right, center)
            inline: (form fields inline)
            fields: [
              { <-- FORM FIELD
                name: (name of field)
                type: (type of form field: section, table, text, textarea, checkbox, radio)
                label: (label of fomr field)
                placeholder: (placeholder of form field)
                help_text: (help text for form field)
                properties: { <-- dynamic properties, different for every field type
                }
              }
            ]
          }
        ]
      }
    ]
  }
 ]
**/

import _ from 'lodash';
import AssistantFormField from './assistantFormField';
export default {
  namespaced: true,
  state: {
    form: [],
    rowIndex: null,
    cellIndex: null,
    fieldIndex: null,
    sectionFieldIndex: null,
    showComponentEditor: false,
    editorHeading: ''
  },
  getters: {
    /**
     *
     * @param state
     * @param getters
     * @param rootState
     * @param rootGetters
     * @returns {*}
     */
    getDoctorID: (state, getters, rootState, rootGetters) => {
      return rootGetters['doctorStore/getDoctorID'];
    },
    /**
     * get doctor form
     * @param state
     * @param getters
     * @param rootState
     * @param rootGetters
     * @returns {*}
     */
    getAssistantForm: (state, getters, rootState, rootGetters) => {
      let f = _.find(state.form, {doctor_id: rootGetters['doctorStore/getDoctorID']});
      if (f) {
        return f.rows;
      } else {
        return [];
      }
    },
    /**
     * set flag for showing form field properties editor
     * @param state
     * @returns {boolean|*}
     */
    showComponentEditor: (state) => {
      return state.showComponentEditor;
    },
    /**
     * return current foeld properties editor heading
     * @param state
     * @returns {string}
     */
    getEditorHeading: (state) => {
      return state.editorHeading;
    }
  },
  mutations: {
    pushForm: (state, form) => {
      state.form = form;
    },
    /**
     * add new row
     * @param state
     * @param doctorID
     */
    addComponent: (state, doctorID) => {
      let form = _.find(state.form, {doctor_id: doctorID});
      if (form) {
        form.rows.push({
          cells: [{
            gr: 8,
            text_align: 'left',
            inline: false,
            fields: []
          }]
        });
      } else {
        state.form.push({
          doctor_id: doctorID,
          rows: [{
            cells: [{
              gr: 8,
              text_align: 'left',
              inline: false,
              fields: []
            }]
          }]
        });
      }
    },
    /**
     * add new cell to row
     * @param state
     * @param payload
     */
    addCell: (state, payload) => {
      let f = _.find(state.form, {doctor_id: payload.doctorID});
      if (f) {
        let fr = f.rows[payload.rowIndex];
        console.log(fr);
        fr.cells.push({
          gr: 8,
          text_align: 'left',
          inline: false,
          fields: []
        });
      }
    },
    /**
     * remove cell from row
     * @param state
     * @param payload
     */
    removeCell: (state, payload) => {
      let f = _.find(state.form, {doctor_id: payload.doctorID});
      if (f) {
        let fr = f.rows[payload.rowIndex];
        if (fr) {
          fr.cells.splice(payload.cellIndex, 1);
        }
      }
    },
    /**
     * remove row
     * @param state
     * @param payload
     */
    removeRow: (state, payload) => {
      let f = _.find(state.form, {doctor_id: payload.doctorID});
      if (f) {
        f.rows.splice(payload.rowIndex, 1);
      }
    },
    /**
     * change cell length
     * @param state
     * @param payload
     */
    changeCellLength: (state, payload) => {
      let f = _.find(state.form, {doctor_id: payload.doctorID});
      if (f) {
        let cellGrVal = f.rows[payload.rowIndex].cells[payload.cellIndex].gr;
        let newVal = cellGrVal + payload.length;
        if (newVal > 0 && newVal <= 24) {
          f.rows[payload.rowIndex].cells[payload.cellIndex].gr = newVal;
        }
      }
    },
    /**
     * set form fields
     * @param state
     * @param payload
     */
    addFormFields: (state, payload) => {
      state.rowIndex = payload.rowIndex;
      state.cellIndex = payload.cellIndex;
    },
    /**
     * change cell inline property
     * true: display form fields inline
     * false: display form fields vertically
     * @param state
     * @param payload
     */
    changeCellProperties: (state, payload) => {
      let f = _.find(state.form, {doctor_id: payload.doctorID});
      if (f) {
        f.rows[payload.rowIndex].cells[payload.cellIndex].inline = payload.inline;
      }
    },
    /**
     * sets current cell field for showing editor
     * or to hide editor
     * @param state
     * @param payload
     */
    setShowComponentEditor: (state, payload) => {
      if (!payload.show) {
        state.rowIndex = null;
        state.cellIndex = null;
        state.fieldIndex = null;
        state.sectionFieldIndex = null;
        state.editorHeading = '';
      }
      state.showComponentEditor = payload.show;
    },
    /**
     * Reset form
     * @param state
     */
    reset (state) {
      state.form = [];
    }
  },
  actions: {
    /**
     * action to add new cell to row
     * @param context
     * @param payload
     */
    addCell (context, payload) {
      payload.doctorID = context.getters.getDoctorID;
      context.commit('addCell', payload);
    },
    /**
     * action to remove row
     * @param context
     * @param payload
     */
    removeRow (context, payload) {
      payload.doctorID = context.getters.getDoctorID;
      context.commit('removeRow', payload);
    },
    /**
     * action to remove cell from row
     * @param context
     * @param payload
     */
    removeCell (context, payload) {
      payload.doctorID = context.getters.getDoctorID;
      context.commit('removeCell', payload);
    },
    /**
     * action to change cell length
     * @param context
     * @param payload
     */
    changeCellLength (context, payload) {
      payload.doctorID = context.getters.getDoctorID;
      context.commit('changeCellLength', payload);
    },
    /**
     * action to set form fields
     * @param context
     * @param payload
     */
    addFormFields (context, payload) {
      context.commit('addFormFields', payload);
    },
    changeCellProperties (context, payload) {
      payload.doctorID = context.getters.getDoctorID;
      context.commit('changeCellProperties', payload);
    }
  },
  modules: {
    assistantFormField: AssistantFormField
  }
};
