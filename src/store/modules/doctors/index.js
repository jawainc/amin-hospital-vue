/**
 * Doctors Data :
 * [
 *  id
 *  name
 *  percentage
 *  totalPercentage
 *  percentageError
 *  assistants: [
 *      id
 *      name
 *      percentage
 *    ]
 *  doctorForm: [],
 *  assistantForm: []
 * ]
 */
import DoctorsForm from './doctorsForm';
import AssistantForm from './assistantForm';
import Doctors from './doctors';
import Assistants from './assistants';
import _ from 'lodash';

export default {
  namespaced: true,
  state: {
    doctorID: null,
    isAssistantForm: false
  },
  getters: {
    getDoctorID: (state) => {
      return state.doctorID;
    },
    getIsAssistantForm: (state) => {
      return state.isAssistantForm;
    },
    totalPercentageError: (state, getters, rootState, rootGetters) => {
      let err = false;
      let text = '';
      rootGetters['doctorStore/doctors/getSelectedDoctors'].forEach(function (item) {
        if (item.percentageError) {
          err = true;
          text = `Computed percentage must be between 0 and 100`;
        }
      });
      return {
        error: err,
        text: text
      };
    },
    feeError: (state, getters, rootState, rootGetters) => {
      let err = false;
      let text = '';
      rootGetters['doctorStore/doctors/getSelectedDoctors'].forEach(function (item) {
        if (item.feeError) {
          err = true;
          text = `Doctor fee must be 0 or greater`;
        }
      });
      return {
        error: err,
        text: text
      };
    },
    getDoctorsForSave: (state, getters, rootState, rootGetters) => {
      let doctors = [];
      let form = rootState.doctorStore.doctorsForm.form;
      let assistantForm = rootState.doctorStore.assistantForm.form;
      rootGetters['doctorStore/doctors/getSelectedDoctors'].forEach(function (item) {
        let doc = {};
        let assisForm = _.cloneDeep(_.find(assistantForm, {doctor_id: item.id})) || [];
        let isAssisForm = !_.isEmpty(assisForm);
        doc.assistant_form = {
          is_form: isAssisForm,
          form: assisForm
        };
        doc.doctor_form = _.cloneDeep(_.find(form, {doctor_id: item.id})) || [];
        doc.id = item.id;
        doc.name = item.name;
        doc.fee = item.fee;
        doc.percentage = item.percentage;
        doc.assistants = item.assistants;
        doctors.push(doc);
      });
      return doctors;
    }
  },
  mutations: {
    setDoctorID (state, doctorID) {
      state.doctorID = doctorID;
    },
    setIsAssistantForm (state, flag) {
      state.isAssistantForm = flag;
    }
  },
  modules: {
    doctors: Doctors,
    assistants: Assistants,
    doctorsForm: DoctorsForm,
    assistantForm: AssistantForm
  }
};
