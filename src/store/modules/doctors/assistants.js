/**
 ******************************
 Assistants List
 ******************************
 [
 {
   doctorId: (id of doctor it belongs to)
   id: (id of assistant)
   name: (name of assistant)
   percentage: (percentage of assistant)
  }
 ]
 */

import _ from 'lodash';

export default {
  namespaced: true,
  mutations: {
    addAssistants (state, payload) {
      payload.doctor = payload.assistants;
    },
    /**
     * changes assistant percentage
     * @param state
     * @param payload
     */
    changePercentage (state, payload) {
      let a = _.find(state.selectedAssistants, _.matches({id: payload.assistantID}));
      if (a) {
        let number = parseInt(payload.assistantPercentage);
        if (_.isFinite(number)) {
          a.percentage = number;
        } else {
          a.percentage = 0;
        }
      }
    }
  },
  actions: {
    setSelectedAssistants (context, assistants) {
      let dID = context.rootState.doctorStore.doctorID;
      if (dID) {
        context.commit('doctorStore/doctors/addAssistants',
          {
            assistants: assistants,
            doctorID: dID
          },
          {
            root: true
          }
        );
      }
    },
    removeAssistant (context, payload) {
      context.commit('doctorStore/doctors/removeAssistant',
        {
          doctorID: payload.doctorID,
          assistantID: payload.id
        },
        {
          root: true
        }
      );
    }
  }
};
