import _ from 'lodash';
export default {
  namespaced: true,
  getters: {
    /**
     * Returns current active form field
     * @param state
     * @param getters
     * @param rootState
     * @param rootGetters
     * @returns {Array}
     */
    getFormField: (state, getters, rootState, rootGetters) => {
      let formField = {};
      if (!_.isNil(rootState.doctorStore.doctorsForm.rowIndex) && !_.isNil(rootState.doctorStore.doctorsForm.cellIndex) && !_.isNil(rootState.doctorStore.doctorsForm.fieldIndex)) {
        formField = rootGetters['doctorStore/doctorsForm/getDoctorForm'][rootState.doctorStore.doctorsForm.rowIndex]
          .cells[rootState.doctorStore.doctorsForm.cellIndex]
          .fields[rootState.doctorStore.doctorsForm.fieldIndex];
        if (!_.isNil(rootState.doctorStore.doctorsForm.sectionFieldIndex)) {
          formField = formField.fields[rootState.doctorStore.doctorsForm.sectionFieldIndex];
        }
      }
      return formField;
    },
    /**
     * Returns current select field options
     * @param state
     * @param getters
     * @returns {Array}
     */
    getSelectOptions: (state, getters) => {
      let form = getters.getFormField;
      if (_.isEmpty(form) && _.isNil(form.properties)) {
        return [];
      } else {
        return form.properties.options;
      }
    },
    /**
     * Returns current table rows
     * @param state
     * @param getters
     * @returns {*}
     */
    getTableRows: (state, getters) => {
      let form = getters.getFormField;
      if (_.isEmpty(form) && _.isNil(form.properties)) {
        return [];
      } else {
        return form.properties.rows;
      }
    },
    /**
     * Returns current table columns
     * @param state
     * @param getters
     * @returns {*}
     */
    getTableColumns: (state, getters) => {
      let form = getters.getFormField;
      if (_.isEmpty(form) && _.isNil(form.properties)) {
        return [];
      } else {
        return form.properties.columns;
      }
    },
    /**
     * Returns if form fold display inline
     * @param state
     * @param getters
     * @returns {boolean}
     */
    getInline: (state, getters) => {
      let form = getters.getFormField;
      if (_.isEmpty(form) && _.isNil(form.properties)) {
        return false;
      } else {
        return form.properties.inline;
      }
    }
  },
  mutations: {
    /**
     * Adds new form field.
     * If section is already there form field will be added to it.
     * If section is added and fields are already there, those fields will be added to new section
     * @param state
     * @param payload
     */
    addFormComponent (state, payload) {
      if (payload.type === 'nestedform') {
        payload.type = 'section';
        payload.isNested = true;
      } else {
        payload.isNested = false;
      }
      let sec = _.find(payload.cell.fields, {type: 'section'});
      // Section found push fields to it
      if (sec) {
        if (payload.type === 'section') {
          // no duplicates for section
        } else {
          // pushing fields to section
          sec.fields.push({
            name: `${payload.type}_${Math.floor((Math.random() * 1000) + 50)}`,
            type: payload.type,
            label: `${payload.type} label`,
            label_urdu: 'ٹیکسٹ لیبل',
            placeholder: '',
            help_text: '',
            properties: {
              language: 'english',
              inline: false,
              rows: [
                {heading: 'row'},
                {heading: 'row'}
              ],
              columns: [
                {heading: 'column'},
                {heading: 'column'},
                {heading: 'column'},
                {heading: 'column'}
              ],
              options: []
            }
          });
        }
      } else if (_.isNil(sec) && payload.type === 'section') {
        if (payload.cell.fields.length > 0) {
          //  fields already there, making copy and inserting into section
          let temp = _.clone(payload.cell.fields);
          payload.cell.fields = [{
            name: `${payload.type}_${Math.floor((Math.random() * 1000) + 50)}`,
            type: payload.type,
            label: payload.isNested ? 'Nested Form Label' : 'Section Label',
            properties: {
              language: 'english',
              inline: false
            },
            fields: temp,
            isNested: payload.isNested,
            button_label: 'button'
          }];
        } else {
          // no section found pushing new section to fields
          payload.cell.fields.push({
            name: `${payload.type}_${Math.floor((Math.random() * 1000) + 50)}`,
            type: payload.type,
            label: payload.isNested ? 'Nested Form Label' : 'Section Label',
            properties: {
              language: 'english',
              inline: false
            },
            fields: [],
            isNested: payload.isNested,
            button_label: 'button'
          });
        }
      } else {
        // addoing new form field
        payload.cell.fields.push({
          name: `${payload.type}_${Math.floor((Math.random() * 1000) + 50)}`,
          type: payload.type,
          label: `${payload.type} label`,
          label_urdu: 'ٹیکسٹ لیبل',
          placeholder: '',
          help_text: '',
          properties: {
            language: 'english',
            inline: false,
            rows: [
              {
                heading: 'row'
              },
              {
                heading: 'row'
              }
            ],
            columns: [
              {heading: 'column'},
              {heading: 'column'},
              {heading: 'column'},
              {heading: 'column'}
            ],
            options: []
          }
        });
      }
    },
    /**
     * Activate form field for editing
     * @param state
     * @param payload
     */
    setEditForFormField (state, payload) {
      payload.rootState.doctorStore.doctorsForm.rowIndex = payload.rowIndex;
      payload.rootState.doctorStore.doctorsForm.cellIndex = payload.cellIndex;
      payload.rootState.doctorStore.doctorsForm.fieldIndex = payload.fieldIndex;
      if (!_.isNil(payload.sectionFieldIndex)) {
        payload.rootState.doctorStore.doctorsForm.sectionFieldIndex = payload.sectionFieldIndex;
      }
      payload.rootState.doctorStore.doctorsForm.editorHeading = payload.editorHeading;
    },
    /**
     * Edit form field
     * @param state
     * @param payload
     */
    editFormComponent (state, payload) {
      _.mapKeys(payload.obj, function (value, key) {
        payload.field[key] = value;
      });
    },
    editFieldProperty (state, payload) {
      payload.field.properties[payload.obj.property] = payload.obj.value;
    },
    changeLanguage (state, payload) {
      payload.field.properties.language = payload.val;
    },
    changeInline (state, payload) {
      payload.field.properties.inline = payload.val;
    },
    addProperty (state, payload) {
      if (payload.type === 'option') {
        // add to select option
        if (_.isNil(payload.field.properties.options)) {
          payload.field.properties.options = [];
        }
        payload.field.properties.options.push({
          label: 'option',
          value: 'option'
        });
      } else if (payload.type === 'row') {
      //  add to table rows
        payload.field.properties.rows.push({
          heading: 'row'
        });
      } else if (payload.type === 'column') {
        //  add to table columns
        payload.field.properties.columns.push({
          heading: 'col'
        });
      }
    },
    editProperty (state, payload) {
      if (payload.type === 'option') {
        payload.option.label = payload.val;
      } else if (payload.type === 'row') {
        payload.row.heading = payload.val;
      } else if (payload.type === 'column') {
        payload.column.heading = payload.val;
      }
    },
    removeProperty (state, payload) {
      payload.props.splice(payload.index, 1);
    },
    removeField (state, payload) {
      payload.splice(payload.index, 1);
    }
  },
  actions: {
    addFormComponent (context, type) {
      return new Promise((resolve, reject) => {
        let doctorForm = _.find(context.rootState.doctorStore.doctorsForm.form, {doctor_id: context.rootGetters['doctorStore/getDoctorID']});
        if (doctorForm) {
          let cell = doctorForm.rows[context.rootState.doctorStore.doctorsForm.rowIndex].cells[context.rootState.doctorStore.doctorsForm.cellIndex];
          let payload = {
            cell: cell,
            type: type
          };
          context.commit('addFormComponent', payload);
        }
        resolve();
      });
    },
    setEditForFormField (context, payload) {
      return new Promise((resolve, reject) => {
        payload.rootState = context.rootState;
        context.commit('setEditForFormField', payload);
        resolve();
      });
    },
    editFormComponent (context, payload) {
      context.commit('editFormComponent', {
        field: context.getters.getFormField,
        obj: payload
      });
    },
    editFieldProperty (context, payload) {
      context.commit('editFieldProperty', {
        field: context.getters.getFormField,
        obj: payload
      });
    },
    changeLanguage (context, payload) {
      return new Promise((resolve, reject) => {
        context.commit('changeLanguage', {
          field: context.getters.getFormField,
          val: payload
        });
        resolve();
      });
    },
    changeInline (context, payload) {
      return new Promise((resolve, reject) => {
        context.commit('changeInline', {
          field: context.getters.getFormField,
          val: payload
        });
        resolve();
      });
    },
    addProperty (context, payload) {
      payload.field = context.getters.getFormField;
      context.commit('addProperty', payload);
    },
    editProperty (context, payload) {
      if (payload.type === 'option') {
        payload.option = context.getters.getSelectOptions[payload.index];
      } else if (payload.type === 'row') {
        payload.row = context.getters.getTableRows[payload.index];
      } else if (payload.type === 'column') {
        payload.column = context.getters.getTableColumns[payload.index];
      }
      context.commit('editProperty', payload);
    },
    removeProperty (context, payload) {
      if (payload.type === 'option') {
        payload.props = context.getters.getSelectOptions;
      } else if (payload.type === 'row') {
        payload.props = context.getters.getTableRows;
      } else if (payload.type === 'column') {
        payload.props = context.getters.getTableColumns;
      }
      context.commit('removeProperty', payload);
    }
  }
};
