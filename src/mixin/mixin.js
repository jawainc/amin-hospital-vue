import validationRules from './validationRules';
import {firebase} from '@/firebase/db';
export default {
  mixins: [validationRules],
  methods: {
    doSignOut () {
      this.$cookie.delete('user-role');
      this.$cookie.delete('department-doctor-id');
      firebase.auth().signOut();
      this.$router.push('/');
    },
    showSuccessMessage (msg) {
      this.$message({
        showClose: true,
        duration: 5000,
        message: msg,
        dangerouslyUseHTMLString: true,
        type: 'success'
      });
    },
    showSaveSuccessMessage () {
      this.$message({
        showClose: true,
        duration: 5000,
        message: 'Saved Successfully',
        type: 'success'
      });
    },
    showErrorMessage (msg) {
      this.$message({
        showClose: true,
        duration: 5000,
        message: msg,
        dangerouslyUseHTMLString: true,
        type: 'error'
      });
    },
    showDeleteMessage () {
      this.$message({
        showClose: true,
        duration: 5000,
        message: 'Deleted Successfully',
        type: 'warning'
      });
    }
  }
};
