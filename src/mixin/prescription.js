import _ from 'lodash';
export default {
  data: function () {
    return {
      prescriptionType: 'doctor', // doctor, assistant
      prescription: {
        assistant: {},
        doctor: {}
      }
    };
  },
  methods: {
    generateFormData (form) {
      if (form.length > 0) {
        form.forEach((row) => {
          row.cells.forEach((cell) => {
            cell.fields.forEach((field) => {
              let type = field.type;
              switch (type) {
                case 'select':
                  this.generateSelect(field);
                  break;
                case 'text':
                  this.generateData(field);
                  break;
                case 'textarea':
                  this.generateData(field);
                  break;
                case 'checkbox':
                  this.generateData(field);
                  break;
                case 'table':
                  this.generateTableData(field);
                  break;
                case 'section':
                  this.generateSection(field);
                  break;
                default:
                  break;
              }
            });
          });
        });
      }
    },
    generateSectionFields (fields, name = null) {
      let index = 0;
      fields.forEach((field) => {
        field.index = index;
        let type = field.type;
        switch (type) {
          case 'text':
            this.generateData(field, name);
            break;
          case 'textarea':
            this.generateData(field, name);
            break;
          case 'checkbox':
            this.generateData(field, name);
            break;
          case 'select':
            this.generateSelect(field, name);
            break;
          case 'table':
            this.generateTableData(field, name);
            break;
          default:
            break;
        }
        index++;
      });
    },
    generateSection (field) {
      if (!field.isNested) {
        this.prescription[this.prescriptionType][field.name] = {
          label: field.properties.language === 'urdu' ? field.label_urdu : field.label,
          fields: []
        };
        this.generateSectionFields(field.fields, field.name);
      } else {
        let ar = this.prescription[this.prescriptionType];
        this.$set(ar, field.name, {});
        let arSet = this.prescription[this.prescriptionType][field.name];
        this.$set(arSet, 'label', field.properties.language === 'urdu' ? field.label_urdu : field.label);
        this.$set(arSet, 'data', []);
        this.$set(arSet, 'fields', {});
        this.generateSectionFields(field.fields, field.name);
      }
    },
    generateData (field, name = null) {
      if (_.isNil(name)) {
        this.$set(this.prescription[this.prescriptionType], field.name, {
          dictionaryName: field.properties.dictionary || null,
          label: field.properties.language === 'urdu' ? field.label_urdu : field.label,
          value: null
        });
      } else {
        this.$set(this.prescription[this.prescriptionType][name].fields, [field.index], {
          name: field.name,
          dictionaryName: field.properties.dictionary || null,
          label: field.properties.language === 'urdu' ? field.label_urdu : field.label,
          value: null
        });
      }
    },
    generateTableData (field, name = null) {
      let dt = [];
      let cols = field.properties.columns;
      field.properties.rows.forEach((r) => {
        dt.push({
          label: r.heading,
          data: this.getColumns(cols)
        });
      });
      if (_.isNil(name)) {
        this.prescription[this.prescriptionType][field.name] = dt;
      } else {
        this.prescription[this.prescriptionType][name].fields[field.name] = dt;
      }
    },
    generateSelect (field, name = null) {
      if (_.isNil(name)) {
        this.$set(this.prescription[this.prescriptionType], field.name, {
          options: field.properties.options,
          label: field.properties.language === 'urdu' ? field.label_urdu : field.label,
          value: null
        });
      } else {
        this.$set(this.prescription[this.prescriptionType][name].fields, field.name, {
          options: field.properties.options,
          label: field.properties.language === 'urdu' ? field.label_urdu : field.label,
          value: null
        });
      }
    },
    getColumns (columns) {
      let temp = {};
      columns.forEach((col) => {
        temp[col.heading] = null;
      });
      return temp;
    }
  }
};
