import _ from 'lodash';
export default {
  data: function () {
    return {
      urdu: {
        /* eslint-disable */
        q: 'ق', w: 'و', e: 'ع', r: 'ر', t: 'ت', y: 'ے', u: 'ء', i: 'ی', o: 'ہ', p: 'پ',
        a: 'ا', s: 'س', d: 'د', f: 'ف', g: 'گ', h: 'ح', j: 'ج', k: 'ک', l: 'ل', ';': '؛',
        z: 'ز', x: 'ش', c: 'چ', v: 'ط', b: 'ب', n: 'ن', m: 'م', ',': '،',

        Q: 'ْْ', W: 'ّ', E: 'ٰ', R: 'ڑ', T: 'ٹ', Y: 'َ', U: 'ئ', I: 'ِ', O: 'ۃ', P: 'ُ',
        A: 'آ', S: 'ص', D: 'ڈ', G: 'غ', H: 'ھ', J: 'ض', K: 'خ',
        Z: 'ذ', X: 'ژ', C: 'ث', V: 'ظ', N: 'ں', M: '٘', '?': '؟'
        /* eslint-enable */
      }
    };
  },
  methods: {
    updateUrduValue (target) {
      let cursorPosition = target.selectionStart;
      let value = target.value;
      let formattedValue = '';
      for (var x = 0; x < value.length; x++) {
        let val = _.get(this.urdu, [value.charAt(x)]);
        if (val !== undefined) {
          formattedValue += val;
        } else {
          formattedValue += value.charAt(x);
        }
      }
      if (formattedValue !== value) {
        target.value = formattedValue;
      }
      target.selectionEnd = cursorPosition;
    },
    getUpdateUrduValue (value) {
      let formattedValue = '';
      for (var x = 0; x < value.length; x++) {
        let val = _.get(this.urdu, [value.charAt(x)]);
        if (val !== undefined) {
          formattedValue += val;
        } else {
          formattedValue += value.charAt(x);
        }
      }
      if (formattedValue !== value) {
        return formattedValue;
      }
      return value;
    },
    getUrduValue (value) {
      let formattedValue = '';
      for (var x = 0; x < value.length; x++) {
        let val = _.get(this.urdu, [value.charAt(x)]);
        if (val !== undefined) {
          formattedValue += val;
        } else {
          formattedValue += value.charAt(x);
        }
      }
      if (formattedValue !== value) {
        return formattedValue;
      }
    }
  }
};
