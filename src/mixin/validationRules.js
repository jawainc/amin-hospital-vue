export default {
  methods: {
    alphaNumeric: function (rule, value, callback) {
      if (!/^[a-z0-9]+$/.test(value)) {
        return callback(new Error('Field must be lowercase alphanumeric characters'));
      } else {
        callback();
      }
    },
    positiveNumber: function (rule, value, callback) {
      if (value) {
        let val = parseInt(value);
        if (!Number.isInteger(val)) {
          return callback(new Error('Please input digits'));
        } else {
          if (val < 0) {
            return callback(new Error('Must be a positive(+) number'));
          } else {
            callback();
          }
        }
      } else {
        callback();
      }
    }
  }
};
