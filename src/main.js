import Vue from 'vue';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en';
import Vue2Filters from 'vue2-filters';
import './styles/style.scss';
import App from './App';
import router from './router';
import Mixin from '@/mixin/mixin';
import VueFirestore from 'vue-firestore';
var VueCookie = require('vue-cookie');

// ignore for warnings
Vue.config.ignoredElements = ['iron-icon'];
Vue.use(ElementUI, { size: 'small', locale });
Vue.use(Vue2Filters);
Vue.use(VueFirestore);
Vue.use(VueCookie);
Vue.mixin(Mixin);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
