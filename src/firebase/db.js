import config from '@/resource/config';
import { firebase } from '@firebase/app';
import '@firebase/firestore';
import '@firebase/auth';

let appConfig = null;
if (process.env.NODE_ENV !== 'development') {
  appConfig = config.fireBaseProductionConfig;
} else {
  appConfig = config.fireBaseConfig;
}
const firebaseApp = firebase.initializeApp(appConfig);
const fakeApp = firebase.initializeApp(appConfig, 'Fake');
let auth = firebaseApp.auth();
let fakeAuth = fakeApp.auth();
let db = firebaseApp.firestore();
const settings = {timestampsInSnapshots: true};
db.settings(settings);

export {db, firebase, auth, fakeAuth};
