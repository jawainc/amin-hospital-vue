import {db, firebase} from '@/firebase/db';

export default {
  name: 'expanses-form',
  data: function () {
    return {
      dataLoading: false,
      formSubmitting: false,
      // patient form data
      form: {
        amount: null,
        department: null,
        details: null
      },
      rules: {
        amount: [
          {required: true, message: 'Amount is required', trigger: 'blur'},
          {required: true, validator: this.positiveNumber, trigger: 'change'}
        ],
        department: [
          {required: true, message: 'Department is required'}
        ],
        details: [
          {required: true, message: 'Expanse detail is required', trigger: 'blur'}
        ]
      }
    };
  },
  firestore () {
    return {
      expenses: db.collection('expenses'),
      departments: db.collection('departments')
    };
  },
  mounted: function () {
    this.dataLoading = true;
    this.$firestore.departments.get().then((docs) => {
      let items = [];
      docs.forEach((doc) => {
        items.push({
          value: doc.id,
          label: doc.data().name
        });
      });
      this.departments = items;
    })
      .catch((error) => {
        this.showErrorMessage(error);
      })
      .then(() => {
        this.dataLoading = false;
      });
  },
  methods: {
    /**
     * submit form
     */
    submit () {
      this.formSubmitting = true;
      let data = {
        amount: parseInt(this.form.amount),
        department: {
          id: this.form.department.value,
          name: this.form.department.label
        },
        details: this.form.details,
        created_at: firebase.firestore.FieldValue.serverTimestamp()
      };
      this.$firestore.expenses.add(data)
        .then((result) => {
          this.showSaveSuccessMessage();
        })
        .catch((e) => {
          this.showErrorMessage(e);
        })
        .then(() => {
          this.formSubmitting = false;
          this.resetForm();
        });
    },
    /**
     * validates before submitting form
     */
    validateBeforeSubmit (name) {
      if (!this.formSubmitting) {
        this.$refs[name].validate((valid) => {
          if (valid) {
            this.submit();
          } else {
            return false;
          }
        });
      }
    },
    resetFields () {
      this.form = {
        amount: null,
        department: null,
        details: null
      };
    },
    resetForm () {
      this.$nextTick(function () {
        // DOM is now updated
        // `this` is bound to the current instance
        this.$refs['expansesForm'].resetFields();
      });
    }
  }
};
