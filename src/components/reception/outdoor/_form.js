import _ from 'lodash';
import moment from 'moment';
import {db, firebase} from '@/firebase/db';

export default {
  name: 'outdoor-form',
  data: function () {
    return {
      // phone numbers shown on typeahead
      phoneData: [],
      // patient name data
      nameData: null,
      // patient data recieved from server
      receivedPhoneData: [],
      receivedNameData: [],
      // doctors data to populate doctor dropdown
      doctorsData: [],
      patientLastVisit: null,
      formSubmitting: false,
      // patient form data
      form: {
        patient_id: null,
        phone: null,
        patient_name: null,
        age: null,
        gender: null,
        nic: null,
        location: null,
        doctor_id: null,
        doctor_name: null,
        fee: 0,
        department_id: null,
        department_name: null,
        discount: 0,
        total: 0
      },
      print: {
        id: null,
        phone: null,
        date: null,
        doctor: null,
        department: null,
        patient: null,
        fee: null
      },
      selectedDoctor: null,
      selectedDoctorIndex: null,
      rules: {
        phone: [
          {required: true, message: 'Phone is required', trigger: 'blur'}
        ],
        patient_name: [
          {required: true, message: 'Name is required', trigger: 'blur'}
        ],
        doctor_name: [
          {required: true, message: 'Doctor is required', trigger: 'blur'}
        ],
        fee: [
          {required: true, message: 'Fee is required', trigger: 'blur'},
          {required: true, validator: this.feeValidator, trigger: 'change'}
        ],
        discount: [
          {required: true, message: 'Discount is required', trigger: 'blur'},
          {validator: this.discountValidator, trigger: 'change'}
        ]
      }
    };
  },
  mounted: function () {
    this.loadDoctors();
  },
  computed: {
    /**
     * calculating total fee
     * fee - discount
     * @returns {*}
     */
    totalFee () {
      let discount = parseInt(this.form.discount);
      let fee = parseInt(this.form.fee);
      if (discount <= fee) {
        return fee - discount;
      } else if (fee >= 0) {
        return fee;
      } else {
        return 0;
      }
    }
  },
  watch: {
    selectedDoctor: function (obj) {
      if (_.isEmpty(obj)) {
        this.form.doctor_id = null;
        this.form.doctor_name = null;
        this.form.fee = 0;
        this.form.department_id = null;
        this.form.department_name = null;
        this.form.total = 0;
        this.form.discount = 0;
      } else {
        this.form.doctor_id = obj.doctorId;
        this.form.doctor_name = obj.doctorName;
        this.form.fee = obj.doctorFee;
        this.form.department_id = obj.departmentId;
        this.form.department_name = obj.departmentName;
        this.form.total = obj.doctorFee;
        this.form.discount = 0;
      }
    },
    selectedDoctorIndex: function (index) {
      this.selectedDoctor = this.doctorsData[index];
    }
  },
  filters: {
    moment: function (date) {
      return moment.unix(date.seconds).format('DD/MM/YYYY');
    }
  },
  methods: {
    /**
     * load doctors on form load
     */
    loadDoctors () {
      db.collection('department_doctors').get().then((docs) => {
        docs.forEach((doc) => {
          let doctor = doc.data();
          this.doctorsData.push({
            departmentDoctorId: doc.id,
            doctorId: doctor.id,
            doctorName: `${doctor.name} (${doctor.department.name})`,
            doctorNameOriginal: doctor.name,
            doctorFee: doctor.fee,
            departmentId: doctor.department.id,
            departmentName: doctor.department.name,
            doctor_percentage: doctor.percentage,
            assistants: doctor.assistants,
            assistant_pending: doctor.assistant_form.is_form
          });
        });
      })
        .catch((error) => {
          this.showErrorMessage(error);
        });
    },
    /**
     * submit form
     */
    async submit () {
      this.formSubmitting = true;
      let data = {
        patient: {
          phone: this.form.phone,
          name: this.form.patient_name,
          age: this.form.age,
          gender: this.form.gender,
          nic: this.form.nic,
          location: this.form.location
        },
        doctor: {
          id: this.form.doctor_id,
          name: this.selectedDoctor.doctorNameOriginal,
          percentage: this.selectedDoctor.doctor_percentage,
          assistants: this.selectedDoctor.assistants,
          fee: this.form.fee,
          department_id: this.form.department_id,
          department_name: this.form.department_name
        },
        total: {
          total: this.totalFee,
          discount: this.form.discount
        }
      };
      let phoneId = await this.checkPhoneExists(data.patient.phone);
      data.patient['phone_id'] = phoneId;
      let patient = await this.addPatient(data.patient);
      this.savePatientEntry(patient, data);
    },
    async addPatient (patient) {
      try {
        let p = db.collection('patients').where('name', '==', patient.name).limit(1).get()
          .then((result) => {
            if (!result.empty) {
              return db.collection('patients')
                .doc(result.docs[0].id).set(patient)
                .then(() => {
                  return result.docs[0].ref;
                });
            } else {
              return db.collection('patients').add(patient);
            }
          });
        return p;
      } catch (e) {
        this.showErrorMessage(e);
      }
    },
    async checkPhoneExists (phone) {
      try {
        let result = await db.collection('phones').where('phone', '==', phone).get();
        if (result.empty) {
          return db.collection('phones').add({phone: phone}).then((ref) => ref.id);
        } else {
          let id = result.docs[0].id;
          return db.collection('phones').doc(id).set({phone: phone}).then((ref) => {
            return id;
          });
        }
      } catch (e) {
        this.showErrorMessage(e);
      }
    },
    savePatientEntry (patient, data) {
      let deptIncome = parseInt(data.total.total);
      let doctorShare = this.calculatePercentage(data.total.total, data.doctor.percentage);
      deptIncome = deptIncome - doctorShare;
      let assistants = [];
      data.doctor.assistants.forEach((assistant) => {
        let assisShare = this.calculatePercentage(data.total.total, assistant.percentage);
        deptIncome = deptIncome - assisShare;
        assistants.push({
          id: assistant.id,
          name: assistant.name,
          amount: assisShare
        });
      });

      db.collection('patient_entries').add({
        department_doctor_id: this.selectedDoctor.departmentDoctorId,
        status: {
          doctor_pending: !this.selectedDoctor.assistant_pending,
          assistant_pending: this.selectedDoctor.assistant_pending,
          complete: false,
          draft: false
        },
        department: {
          id: data.doctor.department_id,
          name: data.doctor.department_name
        },
        patient: {
          id: patient.id,
          path: patient.path,
          phone: data.patient.phone,
          name: data.patient.name
        },
        doctor: {
          id: data.doctor.id,
          name: data.doctor.name
        },
        discount: {
          is_discount: (parseInt(data.total.discount) > 0),
          amount: parseInt(data.total.discount)
        },
        amount: parseInt(data.total.total),
        type: 'outdoor',
        created_at: firebase.firestore.FieldValue.serverTimestamp(),
        updated_at: firebase.firestore.FieldValue.serverTimestamp()
      }).then((doc) => {
        let entryId = doc.id;
        let batch = db.batch();
        let prescriptionAssist = db.collection('patient_entries').doc(entryId).collection('prescription').doc('assistant');
        let prescriptionDoctor = db.collection('patient_entries').doc(entryId).collection('prescription').doc('doctor');
        batch.set(prescriptionAssist, {});
        batch.set(prescriptionDoctor, {});
        let income = db.collection('incomes').doc();
        batch.set(income, {
          patient_entry_id: entryId,
          department: {
            id: data.doctor.department_id,
            name: data.doctor.department_name
          },
          doctor_id: data.doctor.id,
          amount: parseInt(data.total.total),
          created_at: firebase.firestore.FieldValue.serverTimestamp(),
          updated_at: firebase.firestore.FieldValue.serverTimestamp()
        });
        let docShare = db.collection('doctor_shares').doc();
        batch.set(docShare, {
          department_doctor_id: this.selectedDoctor.departmentDoctorId,
          patient_entry_id: entryId,
          department: {
            id: data.doctor.department_id,
            name: data.doctor.department_name
          },
          doctor_id: data.doctor.id,
          amount: doctorShare,
          created_at: firebase.firestore.FieldValue.serverTimestamp(),
          updated_at: firebase.firestore.FieldValue.serverTimestamp()
        });
        let deptShare = db.collection('department_shares').doc();
        batch.set(deptShare, {
          patient_entry_id: entryId,
          department: {
            id: data.doctor.department_id,
            name: data.doctor.department_name
          },
          doctor: {
            id: data.doctor.id,
            name: data.doctor.name
          },
          amount: deptIncome,
          created_at: firebase.firestore.FieldValue.serverTimestamp(),
          updated_at: firebase.firestore.FieldValue.serverTimestamp()
        });
        if (assistants.length > 0) {
          let asstShare = db.collection('assistant_share').doc();
          assistants.forEach((asst) => {
            batch.set(asstShare, {
              department_doctor_id: this.selectedDoctor.departmentDoctorId,
              patient_entry_id: entryId,
              department: {
                id: data.doctor.department_id,
                name: data.doctor.department_name
              },
              doctor: {
                id: data.doctor.id,
                name: data.doctor.name
              },
              amount: asst.amount,
              id: asst.id,
              name: asst.name,
              created_at: firebase.firestore.FieldValue.serverTimestamp(),
              updated_at: firebase.firestore.FieldValue.serverTimestamp()
            });
          });
        }
        batch.commit().then((resp) => {
          this.showSaveSuccessMessage();
          this.printReceipt(data, entryId);
          this.resetForm();
        })
          .catch((error) => {
            console.log(error);
            this.showErrorMessage(error);
          })
          .then(() => {
            this.formSubmitting = false;
          });
      });
    },
    printReceipt (data, entryId) {
      this.print.id = entryId;
      this.print.phone = data.patient.phone;
      this.print.date = moment().format('DD/MM/YYYY HH:mm');
      this.print.doctor = data.doctor.name;
      this.print.department = data.doctor.department_name;
      this.print.patient = data.patient.name;
      this.print.fee = data.total.total;
      this.printHTML(this.$refs.print);
    },
    printHTML () {
      let htmlString = `<div id="print">
                          <div class="text-center"><strong>Amin Hospital</strong></div> 
                          <div>ID: #${this.print.id}</div> 
                          <div>Phone: ${this.print.phone}</div> 
                          <div>Date: ${this.print.date}</div> <hr> 
                          <div>Doctor: ${this.print.doctor} - ${this.print.department}</div> 
                          <div>Patient: ${this.print.patient}</div> <hr> 
                          <div>Fee: <strong>${this.print.fee}</strong></div>
                        </div>`;
      var newIframe = document.createElement('iframe');
      newIframe.width = '1px';
      newIframe.height = '1px';
      newIframe.src = 'about:blank';
      // for IE wait for the IFrame to load so we can access contentWindow.document.body
      newIframe.onload = function () {
        let scriptTag = newIframe.contentWindow.document.createElement('script');
        scriptTag.type = 'text/javascript';
        var script = newIframe.contentWindow.document.createTextNode('function Print(){ window.focus(); window.print(); }');
        scriptTag.appendChild(script);
        newIframe.contentWindow.document.body.innerHTML = htmlString;
        newIframe.contentWindow.document.body.appendChild(scriptTag);
        // for chrome, a timeout for loading large amounts of content
        setTimeout(function () {
          newIframe.contentWindow.Print();
          newIframe.contentWindow.document.body.removeChild(scriptTag);
          newIframe.parentElement.removeChild(newIframe);
        }, 200);
      };
      document.body.appendChild(newIframe);
    },
    calculatePercentage (amount, percent) {
      amount = parseInt(amount);
      percent = parseInt(percent) / 100;
      return parseInt(amount * percent);
    },
    /**
     * validates before submitting form
     */
    validateBeforeSubmit (name) {
      if (!this.formSubmitting) {
        this.$refs[name].validate((valid) => {
          if (valid) {
            this.submit();
          } else {
            return false;
          }
        });
      }
    },
    resetPatient () {
      this.patientLastVisit = null;
      this.form = {
        patient_id: null,
        phone: null,
        patient_name: null,
        age: null,
        gender: null,
        nic: null,
        location: null,
        doctor_id: null,
        doctor_name: null,
        fee: 0,
        department_id: null,
        department_name: null,
        discount: 0,
        total: 0
      };
    },
    resetForm () {
      this.selectedDoctor = null;
      this.selectedDoctorIndex = null;
      this.resetPatient();
      this.$nextTick(function () {
        // DOM is now updated
        // `this` is bound to the current instance
        this.$refs['receptionOutdootForm'].resetFields();
      });
    },
    feeValidator (rule, value, callback) {
      let num = parseInt(value);
      if (Number.isInteger(num) && num >= 0) {
        callback();
      } else {
        return callback(new Error('Fee can be only positive integer value'));
      }
    },
    discountValidator (rule, value, callback) {
      let num = parseInt(value);
      if (Number.isInteger(num) && num >= 0 && num <= this.form.fee) {
        callback();
      } else if (num > this.form.fee) {
        return callback(new Error('Discount cannot be greater than fee amount'));
      } else {
        return callback(new Error('Discount can be only positive integer value'));
      }
    },
    phoneSuggestions (query, cb) {
      db.collection('phones')
        .where('phone', '>=', query)
        .where('phone', '<=', query + '\uf8ff')
        .orderBy('phone').get()
        .then((docs) => {
          this.phoneData = [];
          docs.forEach((doc) => {
            this.phoneData.push({
              phone: doc.data().phone,
              id: doc.id
            });
          });
        })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          cb(this.phoneData);
        });
    },
    handlePhone (obj) {
      this.nameData = obj.phone;
      this.form.patient_name = null;
      this.form.patient_id = null;
      this.form.age = null;
      this.form.gender = null;
      this.form.nic = null;
      this.form.location = null;
      this.patientLastVisit = null;
      this.receivedNameData = [];
    },
    nameSuggestions (query, cb) {
      if (this.phoneData) {
        db.collection('patients').where('phone', '==', this.nameData)
          .get()
          .then((docs) => {
            this.receivedNameData = [];
            docs.forEach((doc) => {
              let data = doc.data();
              data['id'] = doc.id;
              this.receivedNameData.push(data);
            });
          })
          .catch((error) => {
            this.showErrorMessage(error);
          })
          .then(() => {
            cb(this.receivedNameData);
          });
      }
    },
    handleName (obj) {
      obj.last_visit = !obj.last_visit;
      this.form.patient_id = obj.id;
      this.form.age = obj.age;
      this.form.gender = obj.gender;
      this.form.nic = obj.nic;
      this.form.location = obj.location;
      db.collection('patient_entries').orderBy('created_at', 'desc')
        .where('patient.id', '==', obj.id)
        .limit(1).get()
        .then((result) => {
          if (!result.empty) {
            this.patientLastVisit = result.docs[0].data().created_at;
          } else {
            this.patientLastVisit = null;
          }
        });
      /* if (obj.last_visit) {
        this.makeRequest('GET', api.reception.getLastVisit + obj.id).then(result => {
          this.patientLastVisit = result.created_at;
        });
      } else {
        this.patientLastVisit = null;
      } */
    },
    resetPatientID () {
      this.form.patient_id = null;
    }
  }
};
