import {db, firebase} from '@/firebase/db';
import _ from 'lodash';
export default {
  name: 'reception-discount-detail',
  props: ['id'],
  data: function () {
    return {
      formSubmitting: false,
      loading: false,
      data: null
    };
  },
  watch: {
    id: function (newVal, oldVal) {
      this.loadPatient();
    }
  },
  mounted: function () {
    this.loadPatient();
  },
  methods: {
    async submit () {
      this.formSubmitting = true;
      try {
        let patientEntry = await db.collection('patient_entries').doc(this.data.patient_entry_id).get()
          .then((doc) => {
            if (!doc.exists) {
              return Promise.reject(new Error('Invalid Discount'));
            }
            return Promise.resolve(doc.data());
          });
        let departmentDoctor = await db.collection('department_doctors').doc(`${patientEntry.department.id}-${patientEntry.doctor.id}`).get()
          .then((doc) => {
            if (!doc.exists) {
              return Promise.reject(new Error('Invalid Discount'));
            }
            return Promise.resolve({
              id: doc.id,
              doctor: doc.data()
            });
          });
        let assistants = [];
        let discountAmount = parseInt(patientEntry.amount) - parseInt(this.data.discount);
        let deptIncome = discountAmount;
        let doctorShare = this.calculatePercentage(discountAmount, departmentDoctor.doctor.percentage);
        deptIncome = deptIncome - doctorShare;
        departmentDoctor.doctor.assistants.forEach((assistant) => {
          let assisShare = this.calculatePercentage(discountAmount, assistant.percentage);
          deptIncome = deptIncome - assisShare;
          assistants.push({
            id: assistant.id,
            amount: assisShare
          });
        });
        let entryId = this.data.patient_entry_id;
        let batch = db.batch();
        let discountRef = db.collection('discounts').doc(this.data.id);
        batch.update(discountRef, {
          is_discounted: true,
          updated_at: firebase.firestore.FieldValue.serverTimestamp()
        });
        // patient entry
        let patientEntryRef = db.collection('patient_entries').doc(entryId);
        batch.update(patientEntryRef, {
          amount: discountAmount,
          discount: {
            'is_discount': true,
            'amount': this.data.discount
          },
          updated_at: firebase.firestore.FieldValue.serverTimestamp()
        });
        // Income
        let incomeRef = await db.collection('incomes').where('patient_entry_id', '==', entryId).get()
          .then((docs) => {
            docs.forEach((doc) => {
              batch.update(doc.ref, {
                amount: discountAmount,
                updated_at: firebase.firestore.FieldValue.serverTimestamp()
              });
            });
            return Promise.resolve(true);
          });
        // Dept Share
        let departmentRef = await db.collection('department_shares').where('patient_entry_id', '==', entryId).get()
          .then((docs) => {
            docs.forEach((doc) => {
              batch.update(doc.ref, {
                amount: deptIncome,
                updated_at: firebase.firestore.FieldValue.serverTimestamp()
              });
            });
            return Promise.resolve(true);
          });
        // Doctor Share
        let doctorRef = await db.collection('doctor_shares').where('patient_entry_id', '==', entryId).get()
          .then((docs) => {
            docs.forEach((doc) => {
              batch.update(doc.ref, {
                amount: doctorShare,
                updated_at: firebase.firestore.FieldValue.serverTimestamp()
              });
            });
            return Promise.resolve(true);
          });
        // Assistant Shares
        let assist = await db.collection('assistant_share')
          .where('patient_entry_id', '==', entryId).get()
          .then((docs) => {
            docs.forEach((doc) => {
              let asstData = doc.data();
              let findAssistant = _.find(assistants, {id: asstData.id});
              if (findAssistant) {
                batch.update(doc.ref, {
                  amount: findAssistant.amount,
                  updated_at: firebase.firestore.FieldValue.serverTimestamp()
                });
              }
            });
            return Promise.resolve(true);
          });
        if (assist && doctorRef && departmentRef && incomeRef) {
          batch.commit().then(() => {
            this.showSaveSuccessMessage();
            this.$router.push({name: 'Reception.Discount'});
          });
        }
      } catch (e) {
        this.showErrorMessage(e.message);
      } finally {
        this.formSubmitting = false;
      }
    },
    loadPatient () {
      this.loading = true;
      this.reset();
      db.collection('discounts').doc(this.id).get()
        .then((doc) => {
          let dt = doc.data();
          this.data = {
            id: doc.id,
            discount: dt.amount,
            name: dt.name,
            patient_entry_id: dt.patient_entry_id
          };
        })
        .catch(e => this.showErrorMessage(e.message))
        .then(() => { this.loading = false; });
    },
    reset () {
      this.data = null;
    },
    calculatePercentage (amount, percent) {
      amount = parseInt(amount);
      percent = parseInt(percent) / 100;
      return parseInt(amount * percent);
    }
  }
};
