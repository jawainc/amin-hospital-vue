import moment from 'moment';
import {db} from '@/firebase/db';
import _ from 'lodash';
export default {
  name: 'assistant-discount',
  data: function () {
    return {
      Que: [],
      loading: false
    };
  },
  mounted: function () {
    this.loadPatients();
  },
  beforeRouteUpdate: function (to, from, next) {
    if (to.name === 'Reception.Discount') {
      this.loadPatients();
    }
    next();
  },
  filters: {
    moment: function (date) {
      return moment.unix(date).format('h:mm a').toUpperCase();
    }
  },
  firestore () {
    return {
      discount: db.collection('discounts')
    };
  },
  methods: {
    loadPatients () {
      this.loading = true;
      this.$firestore.discount.where('is_discounted', '==', false)
        .orderBy('created_at', 'asc')
        .onSnapshot((querySnapshot) => {
          querySnapshot.docChanges().forEach((snapShot) => {
            console.log(snapShot);
            if (snapShot.type === 'added') {
              let old = _.find(this.Que, { 'id': snapShot.doc.id });
              if (!old) {
                this.Que.push({
                  id: snapShot.doc.id,
                  patient: snapShot.doc.data()
                });
              }
            } else if (snapShot.type === 'removed') {
              this.$delete(this.Que, snapShot.oldIndex);
            }
          });
          this.loading = false;
        });
    }
  }
};
