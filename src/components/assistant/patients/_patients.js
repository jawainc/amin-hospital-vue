import moment from 'moment';
import {db} from '@/firebase/db';
export default {
  name: 'doctor-patients',
  data: function () {
    return {
      tab: 'que',
      assistantQueNum: null,
      allQueNum: null,
      assistantQue: [],
      allQue: [],
      loading: false
    };
  },
  mounted: function () {
    this.loadPatients();
  },
  filters: {
    moment: function (date) {
      return moment.unix(date.seconds).format('h:mm a').toUpperCase();
    }
  },
  firestore () {
    return {
      patients: db.collection('patient_entries')
    };
  },
  methods: {
    loadPatients () {
      // temp doctor
      let depDocId = this.$cookie.get('department-doctor-id');
      this.loading = true;
      this.$firestore.patients
        .where('department_doctor_id', '==', depDocId)
        .where('status.complete', '==', false)
        .where('status.assistant_pending', '==', true)
        .orderBy('created_at', 'asc')
        .onSnapshot((querySnapshot) => {
          querySnapshot.docChanges().forEach((snapShot) => {
            if (snapShot.type === 'added') {
              let data = snapShot.doc.data();
              this.assistantQue.push({
                id: snapShot.doc.id,
                patient: data.patient,
                created_at: data.created_at
              });
            } else if (snapShot.type === 'removed') {
              this.$delete(this.assistantQue, snapShot.oldIndex);
            }
          });
          this.loading = false;
        });
    }
  }
};
