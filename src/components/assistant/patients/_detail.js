import moment from 'moment';
import _ from 'lodash';
import {db, firebase} from '@/firebase/db';
import UrduMixin from '@/mixin/makeUrdu';
import PrescriptionMixin from '@/mixin/prescription';

import TextField from './form/textField';
export default {
  name: 'doctor-patient-detail',
  props: ['id'],
  data: function () {
    return {
      formSubmitting: false,
      loading: false,
      pe_id: null,
      patient: {
        name: null,
        age: null,
        gender: null,
        entryDate: null
      },
      assistant: {
        form: []
      },
      urduComponent: {
        parent: null,
        section: null
      },
      dictionaries: {},
      dictionaryName: null
    };
  },
  components: {
    'text-field': TextField
  },
  mixins: [UrduMixin, PrescriptionMixin],
  watch: {
    id: function (newVal, oldVal) {
      this.loadPatient();
    }
  },
  mounted: function () {
    this.loadPatient();
  },
  filters: {
    moment: function (date) {
      return moment.unix(date.seconds).format('h:mm a').toUpperCase();
    },
    age: function (age) {
      return age ? `${age} yrs` : '';
    },
    patientEntryDate: function (date) {
      return moment.unix(date).format('h:mm a');
    },
    capitalize: function (value) {
      if (!value) return '';
      value = value.toString();
      return value.charAt(0).toUpperCase() + value.slice(1);
    }
  },
  firestore () {
    return {
      department_doctor: db.collection('department_doctors'),
      patient_entry: db.collection('patient_entries'),
      patient: db.collection('patients'),
      dictionary: db.collection('dictionaries')
    };
  },
  methods: {
    submit () {
      this.formSubmitting = true;
      let batch = db.batch();
      _.forEach(this.prescription.assistant, (row) => {
        if (!_.isNil(row.dictionaryName) && !_.isEmpty(row.dictionaryName) && !_.isNil(row.value && !_.isEmpty(row.value))) {
          batch.set(this.$firestore.dictionary.doc(row.dictionaryName), {
            [row.value]: {
              value: row.value
            }
          }, {merge: true});
        } else if (!_.isNil(row.fields) && !_.isNil(row.data) && row.data.length > 0) {
          _.forEach(row.fields, (field, index) => {
            if (!_.isNil(field.dictionaryName)) {
              _.forEach(row.data, (dt) => {
                let contents = dt.contents;
                batch.set(this.$firestore.dictionary.doc(field.dictionaryName), {
                  [contents[index]]: {
                    value: contents[index]
                  }
                }, {merge: true});
              });
            }
          });
        }
      });
      batch.update(this.$firestore.patient_entry.doc(this.id), {
        'status.assistant_pending': false,
        'status.doctor_pending': true,
        updated_at: firebase.firestore.FieldValue.serverTimestamp()
      });
      let prescriptionAssist = db.collection('patient_entries').doc(this.id).collection('prescription').doc('assistant');
      batch.set(prescriptionAssist, this.prescription.assistant);
      batch.commit().then(() => {
        this.showSaveSuccessMessage();
        this.$router.push({name: 'Assistant.Patients'});
      })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.formSubmitting = false;
        });
    },
    async loadPatient () {
      this.loading = true;
      this.reset();
      try {
        let data = await this.$firestore.patient_entry.doc(this.id).get()
          .then((result) => {
            if (result.exists) {
              return Promise.resolve(result.data());
            } else {
              throw new Error('Patient not found');
            }
          });
        let departmentDoctor = await this.$firestore.department_doctor.doc(`${data.department.id}-${data.doctor.id}`).get()
          .then((result) => {
            if (result.exists) {
              return Promise.resolve(result.data());
            } else {
              throw new Error('Doctor not found');
            }
          });
        let patient = await this.$firestore.patient.doc(data.patient.id).get()
          .then((result) => {
            if (result.exists) {
              return Promise.resolve(result.data());
            } else {
              throw new Error('Patient not f ound');
            }
          });
        this.patient.name = patient.name;
        this.patient.age = patient.age;
        this.patient.gender = patient.gender;
        this.patient.entryDate = data.created_at.seconds;
        this.prescriptionType = 'assistant';
        this.assistant.form = departmentDoctor.assistant_form.form.rows || [];
        this.generateFormData(this.assistant.form);
      } catch (e) {
        this.showErrorMessage(e.message);
      } finally {
        this.loading = false;
      }
    },
    editInput (target, lang) {
      if (lang === 'urdu') {
        this.updateUrduValue(target);
      }
    },
    addNestedFormData (name) {
      let fields = this.prescription.assistant[name].fields;
      let dt = [];
      _.forOwn(fields, function (value, key) {
        dt.push(value.value);
        value.value = '';
      });
      if (dt.length > 0) {
        this.prescription.assistant[name].data.push({contents: dt});
      }
    },
    deleteApproved (dt) {
      this.$delete(this.prescription.assistant[dt.name].data, dt.id);
    },
    reset () {
      this.patient.name = null;
      this.patient.age = null;
      this.patient.gender = null;
      this.patient.entryDate = null;
      this.assistant.form = [];
      this.prescription.assistant = {};
    },
    changeValue (val, model) {
    },
    setDictionaryName (name) {
      this.dictionaryName = name;
    },
    dictionarySuggestions (query, cb) {
      if (!_.isNil(this.dictionaryName)) {
        if (_.isNil(this.dictionaries[this.dictionaryName])) {
          this.$firestore.dictionary.doc(this.dictionaryName)
            .get()
            .then((results) => {
              if (results.exists) {
                this.dictionaries[this.dictionaryName] = results.data();
              } else {
                this.dictionaries[this.dictionaryName] = {};
              }
              let suggestions = _.filter(this.dictionaries[this.dictionaryName], (item) => {
                return item.value.indexOf(query) > -1;
              });
              cb(suggestions);
            });
        } else {
          let suggestions = _.filter(this.dictionaries[this.dictionaryName], (item) => {
            return item.value.indexOf(query) > -1;
          });
          cb(suggestions);
        }
      }
    },
    editUrduField (value, dictName, name) {
      this.urduComponent = {
        parent: name,
        section: null
      };
      this.prescription.assistant[name].value = this.getUpdateUrduValue(value);
      if (!_.isNil(dictName)) {
        this.setDictionaryName(dictName);
      }
    },
    editSectionUrduField (obj, dictName, parent, section) {
      this.urduComponent = {
        parent: parent,
        section: section
      };
      this.dictionaryName = null;
      this.prescription.assistant[parent].fields[section].value = this.getUpdateUrduValue(obj.value);
      if (!_.isNil(dictName)) {
        this.setDictionaryName(dictName);
      }
    },
    urduValueSelected (val) {
      if (!_.isNil(this.urduComponent.section)) {
        this.prescription.assistant[this.urduComponent.parent].fields[this.urduComponent.section].value = val.value;
      } else {
        this.prescription.assistant[this.urduComponent.parent].value = val.value;
      }
    },
    handleDelete (obj) {
      this.$confirm(`Are you sure ?`, 'Warning', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        type: 'warning'
      }).then(() => {
        this.$delete(this.prescription.assistant[obj.name].data, obj.id);
      });
    }
  }
};
