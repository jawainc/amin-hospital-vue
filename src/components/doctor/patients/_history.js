import moment from 'moment';
import _ from 'lodash';
import {db} from '@/firebase/db';

export default {
  name: 'patient-history',
  props: ['historyId'],
  data: function () {
    return {
      loading: true,
      data: [],
      patient: null,
      noData: true,
      doctor: {
        form: []
      },
      assistant: {
        form: []
      }
    };
  },
  watch: {
    id: function () {
      this.loadHistory();
    }
  },
  mounted: function () {
    this.loadHistory();
  },
  filters: {
    moment: function (date) {
      return moment.unix(date).format('h:mm a').toUpperCase();
    },
    age: function (age) {
      return age ? `${age}yrs` : '';
    },
    patientEntryDate: function (date) {
      return moment.unix(date).format('LLL');
    },
    capitalize: function (value) {
      if (!value) return '';
      value = value.toString();
      return value.charAt(0).toUpperCase() + value.slice(1);
    }
  },
  firestore () {
    return {
      department_doctor: db.collection('department_doctors'),
      patient_entry: db.collection('patient_entries'),
      patient: db.collection('patients'),
      discount: db.collection('discounts'),
      dictionary: db.collection('dictionaries')
    };
  },
  methods: {
    async loadHistory () {
      console.log(this.historyId);
      this.loading = true;
      if (!_.isNil(this.historyId)) {
        try {
          this.data = [];
          let patientEntries = await this.$firestore.patient_entry
            .where('doctor.id', '==', this.historyId.doctor.id)
            .where('patient.id', '==', this.historyId.patient_id)
            .where('department.id', '==', this.historyId.department.id)
            .where('type', '==', 'outdoor')
            .where('status.complete', '==', true)
            .orderBy('created_at', 'desc')
            .get()
            .then((result) => {
              console.log(result);
              return Promise.resolve(result.docs);
            });
          _.forEach(patientEntries, (doc) => {
            this.$firestore.patient_entry
              .doc(doc.id).collection('prescription')
              .get()
              .then((results) => {
                if (!results.empty) {
                  let ret = {};
                  results.docs.forEach((doc) => {
                    if (doc.exists && !_.isNil(doc.data()) && !_.isEmpty(doc.data())) {
                      ret[doc.id] = doc.data();
                    } else {
                      ret[doc.id] = {};
                    }
                  });
                  return Promise.resolve(ret);
                } else {
                  return Promise.resolve({
                    assistant: [],
                    doctor: []
                  });
                }
              })
              .then((prescription) => {
                let dt = doc.data();
                this.data.push({
                  created_at: dt.created_at,
                  prescription: prescription,
                  forms: {
                    assistant_form: this.historyId.assistant_form,
                    doctor_form: this.historyId.doctor_form
                  }
                });
              });
          });
        } catch (e) {
          this.showErrorMessage(e.message);
        } finally {
          console.log(this.data);
          if (this.data.length > 0) {
            this.noData = false;
          }
          this.loading = false;
        }
      }
    },
    closeHistory () {
      this.patient = null;
      this.data = [];
      this.$emit('closeHistory');
    }
  }
};
