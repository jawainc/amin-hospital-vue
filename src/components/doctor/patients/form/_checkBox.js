import {mapActions, mapGetters} from 'vuex';
export default {
  name: 'checkbox-field',
  props: ['data'],
  computed: {
    ...mapGetters({
      getDoctorFieldValue: 'prescriptionStore/doctorForm/getFieldValue'
    }),
    checkBox: {
      get: function () {
        let vals = {
          name: this.data.field.name,
          sectionFieldIndex: this.data.sectionFieldIndex,
          rowIndex: this.data.rowIndex,
          cellIndex: this.data.cellIndex,
          fieldIndex: this.data.fieldIndex
        };
        if (this.data.type === 'doctor') {
          return this.getDoctorFieldValue(vals);
        }
      },
      set: function (newVal) {
        this.addValue({
          type: this.data.type,
          name: this.data.field.name,
          value: newVal,
          sectionFieldIndex: this.data.sectionFieldIndex,
          rowIndex: this.data.rowIndex,
          cellIndex: this.data.cellIndex,
          fieldIndex: this.data.fieldIndex
        });
      }
    }
  },
  methods: {
    ...mapActions({
      addValue: 'prescriptionStore/addValue'
    })
  }
};
