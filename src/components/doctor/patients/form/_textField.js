import {mapActions, mapGetters} from 'vuex';
import urdu from '@/mixin/makeUrdu';
export default {
  name: 'text-field',
  props: ['data'],
  mixins: [urdu],
  computed: {
    ...mapGetters({
      getDoctorFieldValue: 'prescriptionStore/doctorForm/getFieldValue'
    }),
    textField: {
      get: function () {
        let vals = {
          name: this.data.field.name,
          sectionFieldIndex: this.data.sectionFieldIndex,
          rowIndex: this.data.rowIndex,
          cellIndex: this.data.cellIndex,
          fieldIndex: this.data.fieldIndex
        };
        if (this.data.type === 'doctor') {
          return this.getDoctorFieldValue(vals);
        }
      },
      set: function (newVal) {
        this.addValue({
          type: this.data.type,
          name: this.data.field.name,
          value: this.getLangValue(newVal),
          sectionFieldIndex: this.data.sectionFieldIndex,
          rowIndex: this.data.rowIndex,
          cellIndex: this.data.cellIndex,
          fieldIndex: this.data.fieldIndex
        });
      }
    }
  },
  methods: {
    ...mapActions({
      addValue: 'prescriptionStore/addValue'
    }),
    addTextFieldValue (target, lang) {
      this.addValue({
        type: this.data.type,
        name: this.data.field.name,
        value: this.getLangValue(target, lang)
      });
    },
    getLangValue (value) {
      if (this.data.field.properties.language === 'urdu') {
        return this.getUrduValue(value);
      } else {
        return value;
      }
    }
  }
};
