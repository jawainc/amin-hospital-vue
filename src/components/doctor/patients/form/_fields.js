// import {mapGetters, mapActions, mapMutations} from 'vuex';
import TextField from './textField.vue';
// import Select from './select.vue';
import TextArea from './textArea.vue';
import Table from './tableField.vue';
import CheckBox from './checkBox.vue';
export default {
  name: 'fields',
  props: ['data'],
  components: {
    'check-box': CheckBox,
    'text-field': TextField,
    'text-area': TextArea,
    'table-field': Table
  }
  // components: {
  //   'text-field': TextField,
  //   'text-area': TextArea,
  //   'select-field': Select,
  //   'checkbox-field': CheckBox,
  //   'table-field': Table
  // },
  // methods: {
  //   ...mapGetters({
  //     getIsAssistantForm: 'doctorStore/getIsAssistantForm'
  //   }),
  //   ...mapMutations({
  //     setShowComponentEditor: 'doctorStore/doctorsForm/setShowComponentEditor',
  //     setAssistantShowComponentEditor: 'doctorStore/assistantForm/setShowComponentEditor'
  //   }),
  //   ...mapActions({
  //     setEditForFormField: 'doctorStore/doctorsForm/doctorFormField/setEditForFormField',
  //     setAssistantEditForFormField: 'doctorStore/assistantForm/assistantFormField/setEditForFormField'
  //   }),
  //   editFormComponent () {
  //     if (this.getIsAssistantForm()) {
  //       this.callAssistantEditFormComponent();
  //     } else {
  //       this.callEditFormComponent();
  //     }
  //   },
  //   callEditFormComponent () {
  //     this.setEditForFormField({
  //       rowIndex: this.data.rowIndex,
  //       cellIndex: this.data.cellIndex,
  //       fieldIndex: this.data.fieldIndex,
  //       sectionFieldIndex: this.data.sectionFieldIndex,
  //       editorHeading: 'Section'
  //     }).then(() => {
  //       this.$nextTick(function () {
  //         this.setShowComponentEditor({show: true});
  //       });
  //     });
  //   },
  //   callAssistantEditFormComponent () {
  //     this.setAssistantEditForFormField({
  //       rowIndex: this.data.rowIndex,
  //       cellIndex: this.data.cellIndex,
  //       fieldIndex: this.data.fieldIndex,
  //       sectionFieldIndex: this.data.sectionFieldIndex,
  //       editorHeading: 'Section'
  //     }).then(() => {
  //       this.$nextTick(function () {
  //         this.setAssistantShowComponentEditor({show: true});
  //       });
  //     });
  //   }
  // }
};
