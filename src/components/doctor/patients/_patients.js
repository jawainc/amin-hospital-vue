import moment from 'moment';
import {db} from '@/firebase/db';
export default {
  name: 'doctor-patients',
  data: function () {
    return {
      tab: 'que',
      doctorQueNum: null,
      assistantQueNum: null,
      allQueNum: null,
      doctorQue: [],
      assistantQue: [],
      allQue: [],
      loading: true
    };
  },
  mounted: function () {
    this.loadPatients();
  },
  filters: {
    moment: function (date) {
      return moment.unix(date.seconds).format('h:mm a').toUpperCase();
    }
  },
  firestore () {
    return {
      patients: db.collection('patient_entries')
    };
  },
  methods: {
    loadPatients () {
      // temp doctor
      let depDocId = this.$cookie.get('department-doctor-id');
      this.loading = true;
      this.$firestore.patients
        .where('department_doctor_id', '==', depDocId)
        .where('status.complete', '==', false)
        .where('type', '==', 'outdoor')
        .orderBy('created_at', 'asc')
        .onSnapshot((querySnapshot) => {
          querySnapshot.docChanges().forEach((snapShot) => {
            let data = snapShot.doc.data();
            let pushData = {
              id: snapShot.doc.id,
              data: data
            };

            if (snapShot.type === 'added') {
              this.allQueNum++;
              this.allQue.push(pushData);
              if (data.status.doctor_pending) {
                this.doctorQueNum++;
                this.doctorQue.push(pushData);
              }
              if (data.status.assistant_pending) {
                this.assistantQueNum++;
                this.assistantQue.push(pushData);
              }
            } else if (snapShot.type === 'removed') {
              this.allQueNum--;
              this.$delete(this.allQue, snapShot.oldIndex);
              for (let i = 0; i < this.doctorQue.length; i++) {
                if (this.doctorQue[i].id === pushData.id) {
                  this.$delete(this.doctorQue, i);
                  this.doctorQueNum--;
                  break;
                }
              }
            } else if (snapShot.type === 'modified') {
              if (data.status.draft) {
                for (let i = 0; i < this.doctorQue.length; i++) {
                  if (this.doctorQue[i].id === pushData.id) {
                    this.doctorQue[i].data.status['draft'] = true;
                    break;
                  }
                }
              } else if (data.status.doctor_pending) {
                for (let i = 0; i < this.assistantQue.length; i++) {
                  if (this.assistantQue[i].id === pushData.id) {
                    this.$delete(this.assistantQue, i);
                    this.doctorQueNum++;
                    this.doctorQue.push(pushData);
                    this.assistantQueNum--;
                    break;
                  }
                }
              }
            }
          });
          this.loading = false;
        });
    }
  }
};
