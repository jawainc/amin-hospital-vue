import moment from 'moment';
import _ from 'lodash';
import {db, firebase} from '@/firebase/db';
import UrduMixin from '@/mixin/makeUrdu';
import PrescriptionMixin from '@/mixin/prescription';
import History from './history';

export default {
  name: 'doctor-patient-detail',
  props: ['id'],
  components: {
    history: History
  },
  data: function () {
    return {
      formSubmitting: false,
      loading: false,
      history: false,
      pe_id: null,
      patientId: null,
      historyId: null,
      patientDiscount: 0,
      isDiscounted: false,
      patientEntryData: null,
      departmentDoctor: null,
      patient: {
        name: null,
        age: null,
        gender: null,
        entryDate: null
      },
      doctor: {
        form: []
      },
      assistant: {
        form: []
      },
      dictionaries: {},
      dictionaryName: null
    };
  },
  mixins: [UrduMixin, PrescriptionMixin],
  watch: {
    id: function (newVal, oldVal) {
      this.loadPatient();
    }
  },
  mounted: function () {
    this.loadPatient();
  },
  filters: {
    moment: function (date) {
      return moment.unix(date).format('h:mm a').toUpperCase();
    },
    age: function (age) {
      return age ? `${age}yrs` : '';
    },
    patientEntryDate: function (date) {
      return moment.unix(date).format('LLL');
    },
    capitalize: function (value) {
      if (!value) return '';
      value = value.toString();
      return value.charAt(0).toUpperCase() + value.slice(1);
    }
  },
  firestore () {
    return {
      department_doctor: db.collection('department_doctors'),
      patient_entry: db.collection('patient_entries'),
      patient: db.collection('patients'),
      discount: db.collection('discounts'),
      dictionary: db.collection('dictionaries')
    };
  },
  methods: {
    submit (method = 'save') {
      let batch = db.batch();
      _.forEach(this.prescription.assistant, (row) => {
        if (!_.isNil(row.dictionaryName) && !_.isEmpty(row.dictionaryName) && !_.isNil(row.value && !_.isEmpty(row.value))) {
          batch.set(this.$firestore.dictionary.doc(row.dictionaryName), {
            [row.value]: {
              value: row.value
            }
          }, {merge: true});
        } else if (!_.isNil(row.fields) && !_.isNil(row.data) && row.data.length > 0) {
          _.forEach(row.fields, (field, index) => {
            if (!_.isNil(field.dictionaryName)) {
              _.forEach(row.data, (dt) => {
                let contents = dt.contents;
                batch.set(this.$firestore.dictionary.doc(field.dictionaryName), {
                  [contents[index]]: {
                    value: contents[index]
                  }
                }, {merge: true});
              });
            }
          });
        }
      });
      _.forEach(this.prescription.doctor, (row) => {
        if (!_.isNil(row.dictionaryName) && !_.isNil(row.value) && !_.isNil(row.value && !_.isEmpty(row.value))) {
          batch.set(this.$firestore.dictionary.doc(row.dictionaryName), {
            [row.value]: {
              value: row.value
            }
          }, {merge: true});
        } else if (!_.isNil(row.fields) && !_.isNil(row.data) && row.data.length > 0) {
          _.forEach(row.fields, (field, index) => {
            if (!_.isNil(field.dictionaryName)) {
              _.forEach(row.data, (dt) => {
                let contents = dt.contents;
                batch.set(this.$firestore.dictionary.doc(field.dictionaryName), {
                  [contents[index]]: {
                    value: contents[index]
                  }
                }, {merge: true});
              });
            }
          });
        }
      });
      batch.update(this.$firestore.patient.doc(this.patientId), {
        name: this.patient.name,
        age: this.patient.age,
        gender: this.patient.gender
      });
      batch.set(this.$firestore.patient_entry
        .doc(this.id).collection('prescription').doc('assistant'), this.prescription.assistant);
      batch.set(this.$firestore.patient_entry
        .doc(this.id).collection('prescription').doc('doctor'), this.prescription.doctor);
      if (method === 'save') {
        batch.update(this.$firestore.patient_entry.doc(this.id), {
          'status.complete': true,
          'patient.name': this.patient.name,
          updated_at: firebase.firestore.FieldValue.serverTimestamp()
        });
      } else if (method === 'draft') {
        batch.update(this.$firestore.patient_entry.doc(this.id), {
          'status.draft': true,
          'patient.name': this.patient.name,
          updated_at: firebase.firestore.FieldValue.serverTimestamp()
        });
      }
      batch.commit().then(() => {
        this.showSaveSuccessMessage();
        if (method === 'save') {
          this.$router.push({name: 'Doctor.Patients'});
        }
      })
        .catch((error) => {
          console.log(error);
          console.log(error.message);
          this.showErrorMessage(error.message);
        })
        .then(() => {
          this.formSubmitting = false;
        });
    },
    async loadPatient () {
      this.history = false;
      this.loading = true;
      this.reset();
      try {
        let data = await this.$firestore.patient_entry.doc(this.id).get()
          .then((result) => {
            if (result.exists) {
              return Promise.resolve(result.data());
            } else {
              throw new Error('Patient not found');
            }
          });
        this.departmentDoctor = `${data.department.id}-${data.doctor.id}`;
        let departmentDoctor = await this.$firestore.department_doctor.doc(this.departmentDoctor).get()
          .then((result) => {
            if (result.exists) {
              return Promise.resolve(result.data());
            } else {
              throw new Error('Doctor not found');
            }
          });
        this.patientId = data.patient.id;
        let patient = await this.$firestore.patient.doc(data.patient.id).get()
          .then((result) => {
            if (result.exists) {
              return Promise.resolve(result.data());
            } else {
              throw new Error('Patient not found');
            }
          });
        let prescriptionData = await this.$firestore.patient_entry.doc(this.id).collection('prescription').get()
          .then((results) => {
            if (!results.empty) {
              let ret = {};
              results.docs.forEach((doc) => {
                if (doc.exists && !_.isNil(doc.data()) && !_.isEmpty(doc.data())) {
                  ret[doc.id] = doc.data();
                } else {
                  ret[doc.id] = {};
                }
              });
              return Promise.resolve(ret);
            } else {
              return Promise.resolve({
                assistant: [],
                doctor: []
              });
            }
          });
        this.patientEntryData = data;
        this.patient.name = patient.name;
        this.patient.age = patient.age;
        this.patient.gender = patient.gender;
        this.patient.entryDate = data.created_at.seconds;
        this.patientDiscount = data.discount.amount;
        this.isDiscounted = data.discount.is_discount;
        this.assistant.form = departmentDoctor.assistant_form.form.rows || [];
        this.doctor.form = departmentDoctor.doctor_form.rows || [];
        this.prescription = prescriptionData;
        this.prescriptionType = 'doctor';
        if (_.isEmpty(this.prescription.doctor)) {
          this.generateFormData(this.doctor.form);
        }
      } catch (e) {
        this.showErrorMessage(e.message);
      } finally {
        this.loading = false;
      }
    },
    editInput (target, lang) {
      if (lang === 'urdu') {
        this.updateUrduValue(target);
      }
    },
    addNestedFormData (name, type) {
      let fields = this.prescription[type][name].fields;
      let dt = [];
      _.forOwn(fields, function (value, key) {
        dt.push(value.value);
        value.value = '';
      });
      if (dt.length > 0) {
        this.prescription[type][name].data.push({contents: dt});
      }
    },
    deleteApproved (dt) {
      this.$delete(this.prescription.assistant[dt.name].data, dt.id);
    },
    reset () {
      this.patient.name = null;
      this.patient.age = null;
      this.patient.gender = null;
      this.patient.entryDate = null;
      this.assistant.form = [];
      this.prescription.assistant = {};
    },
    changeValue (val, model) {
    },
    setDictionaryName (name) {
      this.dictionaryName = name;
    },
    dictionarySuggestions (query, cb) {
      if (!_.isNil(this.dictionaryName)) {
        if (_.isNil(this.dictionaries[this.dictionaryName])) {
          this.$firestore.dictionary.doc(this.dictionaryName)
            .get()
            .then((results) => {
              if (results.exists) {
                this.dictionaries[this.dictionaryName] = results.data();
              } else {
                this.dictionaries[this.dictionaryName] = {};
              }
              let suggestions = _.filter(this.dictionaries[this.dictionaryName], (item) => {
                return item.value.indexOf(query) > -1;
              });
              cb(suggestions);
            });
        } else {
          let suggestions = _.filter(this.dictionaries[this.dictionaryName], (item) => {
            return item.value.indexOf(query) > -1;
          });
          cb(suggestions);
        }
      }
    },
    editUrduField (value, dictName, name, type) {
      this.urduComponent = {
        parent: name,
        section: null,
        type: type
      };
      this.prescription[type][name].value = this.getUpdateUrduValue(value);
      if (!_.isNil(dictName)) {
        this.setDictionaryName(dictName);
      }
    },
    editSectionUrduField (obj, dictName, parent, section, type) {
      this.urduComponent = {
        parent: parent,
        section: section,
        type: type
      };
      this.dictionaryName = null;
      this.prescription[type][parent].fields[section].value = this.getUpdateUrduValue(obj.value);
      if (!_.isNil(dictName)) {
        this.setDictionaryName(dictName);
      }
    },
    urduValueSelected (val) {
      if (!_.isNil(this.urduComponent.section)) {
        this.prescription[this.urduComponent.type][this.urduComponent.parent].fields[this.urduComponent.section].value = val.value;
      } else {
        this.prescription[this.urduComponent.type][this.urduComponent.parent].value = val.value;
      }
    },
    handleDelete (obj, type) {
      this.$confirm(`Are you sure ?`, 'Warning', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        type: 'warning'
      }).then(() => {
        this.$delete(this.prescription[type][obj.name].data, obj.id);
      });
    },
    showHistory () {
      this.historyId = {
        patient_id: this.patientId,
        patient: this.patient,
        doctor: this.patientEntryData.doctor,
        department: this.patientEntryData.department,
        doctor_form: this.doctor.form,
        assistant_form: this.assistant.form
      };
      this.history = true;
    },
    hideHistory () {
      this.history = false;
    },
    addDiscount () {
      if (this.isDiscounted) {
        this.showErrorMessage('Already Discounted');
      } else {
        this.$prompt('Add discount', 'Patient Discount', {
          confirmButtonText: 'Save',
          cancelButtonText: 'Cancel',
          inputPattern: /^\d+$/,
          inputErrorMessage: 'Only numbers allowed'
        }).then(value => {
          let data = {
            patient_entry_id: this.id,
            amount: value.value,
            name: `${this.patientEntryData.patient.name}(${this.patientEntryData.department.name})`,
            is_discounted: false,
            created_at: firebase.firestore.FieldValue.serverTimestamp(),
            updated_at: firebase.firestore.FieldValue.serverTimestamp()
          };
          this.$firestore.discount.doc(`discount-${this.id}`).get()
            .then((docRef) => {
              if (docRef.exists) {
                throw new Error('Already Discounted');
              } else {
                this.$firestore.discount.doc(`discount-${this.id}`).set(data)
                  .then(() => this.showSaveSuccessMessage())
                  .catch((e) => { throw e; });
              }
            }).catch((error) => {
              this.showErrorMessage(error.message || error);
            });
        });
      }
    },
    flatPrescription () {
      let presc = {
        assistant: {},
        doctor: {}
      };
      _.forEach(this.prescription.assistant, (value, key) => {
        presc.assistant[key] = value;
      });
      _.forEach(this.prescription.doctor, (value, key) => {
        presc.doctor[key] = value;
      });
      return presc;
    }
  }
};
