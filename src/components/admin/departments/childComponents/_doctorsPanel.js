import {db} from '@/firebase/db';
import {mapMutations} from 'vuex';

export default {
  name: 'doctors-panel',
  data: function () {
    return {
      dataLoading: false,
      items: [],
      selected: []
    };
  },
  mounted: function () {
    this.loadDoctors();
  },
  methods: {
    ...mapMutations({
      setSelectedDoctors: 'doctorStore/doctors/setSelectedDoctors'
    }),
    loadDoctors () {
      this.dataLoading = true;
      db.collection('doctors').get().then((docs) => {
        let temp = [];
        docs.forEach((doc) => {
          temp.push({
            id: doc.id,
            name: doc.data().name,
            fee: 0,
            percentage: 0,
            totalPercentage: 0,
            percentageError: false,
            feeError: false,
            assistants: [],
            doctorForm: [],
            assistantForm: []
          });
        });
        this.items = temp;
      })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.dataLoading = false;
        });
    },
    handleSelectionChange (val) {
      this.selected = val;
    },
    handleCradHeaderCommand (command) {
      if (command === 'addSelected') {
        this.setSelectedDoctors(this.selected);
      }
      if (command === 'close') {
        this.$emit('closeDoctorsPanel');
      }
    },
    // ToDo add search functionality
    search (val) {
      console.log(val);
    }
  }
};
