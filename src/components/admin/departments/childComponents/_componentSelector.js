import {mapActions} from 'vuex';
export default {
  name: 'component-selector',
  props: ['show'],
  methods: {
    ...mapActions({
      addFormComponent: 'doctorStore/doctorsForm/doctorFormField/addFormComponent'
    }),
    close (done) {
      this.$emit('update:show', false);
    },
    addComponent (type) {
      this.addFormComponent(type).then(() => {
        this.close(true);
      });
    }
  }
};
