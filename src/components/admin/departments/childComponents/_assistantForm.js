import {mapGetters, mapActions} from 'vuex';
import ComponentSelector from './componentAssistantSelector.vue';
import ComponentEditor from './componentAssistantEditor.vue';
import TextField from './formElements/textField.vue';
import TextArea from './formElements/textArea.vue';
import Select from './formElements/select.vue';
import CheckBox from './formElements/checkBox.vue';
import Table from './formElements/tableField.vue';
import SectionField from './formElements/section.vue';
export default {
  name: 'assistant-form',
  data: function () {
    return {
      dataLoading: false,
      showAssistantComponentSelector: false,
      editorHeading: ''
    };
  },
  components: {
    componentAssistantSelector: ComponentSelector,
    componentAssistantEditor: ComponentEditor,
    'text-field': TextField,
    'text-area': TextArea,
    'select-field': Select,
    'checkbox-field': CheckBox,
    'table-field': Table,
    sectionField: SectionField
  },
  computed: {
    ...mapGetters({
      doctorID: 'doctorStore/getDoctorID',
      assistantForm: 'doctorStore/assistantForm/getAssistantForm'
    }),
    enableDictinary: {
      get () {
        if (this.$store.getters.getElement.enableDictinary) {
          return this.$store.getters.getElement.enableDictinary;
        } else {
          return false;
        }
      },
      set (value) {
        this.$store.commit('setElement', {
          key: 'enableDictinary',
          value: value
        });
      }
    }
  },
  methods: {
    ...mapActions({
      addCell: 'doctorStore/assistantForm/addCell',
      removeCell: 'doctorStore/assistantForm/removeCell',
      removeRow: 'doctorStore/assistantForm/removeRow',
      changeCellLength: 'doctorStore/assistantForm/changeCellLength',
      addFormFields: 'doctorStore/assistantForm/addFormFields',
      changeCellProperties: 'doctorStore/assistantForm/changeCellProperties'
    }),
    setElement (key, val) {
      this.$store.commit('setElement', {
        key: key,
        value: val
      });
    },
    displayFormFields (args) {
      this.addFormFields(args);
      this.showAssistantComponentSelector = true;
    },
    addComponent () {
      this.$store.commit('doctorStore/assistantForm/addComponent', this.doctorID);
    },
    editFormComponent (editorHeading, fieldSectionIndex, fieldIndex, indexCell, rowIndex) {
      this.editorHeading = editorHeading;
      this.setEditForFormField({
        rowIndex: rowIndex,
        cellIndex: indexCell,
        fieldIndex: fieldIndex,
        sectionFieldIndex: fieldSectionIndex
      });
      this.showAssistantComponentEditor = true;
    },
    closeComponentEditor () {
      this.$store.commit('removeElementForEdit');
      this.showAssistantComponentEditor = false;
    },
    close () {
      this.$emit('closeAssistantForm');
    }
  }
};
