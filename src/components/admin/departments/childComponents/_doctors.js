import DeleteButton from '@/components/shared/modals/delete-confirm-local';
import {mapGetters, mapMutations, mapActions} from 'vuex';
export default {
  name: 'department-doctors',
  components: {
    'delete-button': DeleteButton
  },
  computed: {
    ...mapGetters({
      totalPercentageError: 'doctorStore/totalPercentageError',
      feeError: 'doctorStore/feeError',
      getDoctors: 'doctorStore/doctors/getSelectedDoctors',
      getDoctorID: 'doctorStore/getDoctorID'
    }),
    getSelectedDoctors: function () {
      return this.getDoctors;
    }
  },
  methods: {
    ...mapMutations({
      changePercentage: 'doctorStore/doctors/changePercentage',
      changeFee: 'doctorStore/doctors/changeFee',
      changeAssistantPercentage: 'doctorStore/doctors/changeAssistantPercentage',
      removeDoctor: 'doctorStore/doctors/removeDoctor',
      setDoctorID: 'doctorStore/setDoctorID',
      setIsAssistantForm: 'doctorStore/setIsAssistantForm'
    }),
    ...mapActions({
      removeAssistant: 'doctorStore/assistants/removeAssistant'
    }),
    showDoctorsPanel () {
      this.$emit('showDoctorsPanel');
    },
    showAssistantsPanel (docId) {
      this.setDoctorID(docId);
      this.$emit('showAssistantsPanel');
    },
    showDoctorsForm (docId) {
      console.log(docId);
      this.setDoctorID(docId);
      this.setIsAssistantForm(false);
      this.$emit('showDoctorsForm');
    },
    showAssistantForm (docId) {
      this.setDoctorID(docId);
      this.setIsAssistantForm(true);
      this.$emit('showAssistantForm');
    },
    deleteApproved (index) {
      this.removeDoctor(index);
    },
    deleteApprovedAssistant (obj) {
      this.removeAssistant(obj);
    },
    doctorChangePercentage (target, id) {
      this.changePercentage({
        doctorID: id,
        val: target.value
      });
    },
    doctorChangeFee (target, id) {
      this.changeFee({
        doctorID: id,
        val: target.value
      });
    },
    assistantChangePercentage (target, dID, aID) {
      this.changeAssistantPercentage({
        doctorID: dID,
        assistantID: aID,
        val: target.value
      });
    },
    handleCradHeaderCommand (command) {
      if (command.action === 'showAssistantsPanel') {
        this.showAssistantsPanel(command.id);
      } else if (command.action === 'showAssistantForm') {
        this.showAssistantForm(command.id);
      }
    },
    handleDoctorDelete (index) {
      this.$confirm('This will remove doctor. Continue?', 'Warning', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        type: 'warning'
      }).then(() => {
        this.deleteApproved(index);
      });
    },
    handleAssistantDelete (obj) {
      this.$confirm('This will remove assistant. Continue?', 'Warning', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        type: 'warning'
      }).then(() => {
        this.deleteApprovedAssistant(obj);
      });
    }
  }
};
