import _ from 'lodash';
import UrduMixin from '@/mixin/makeUrdu';
import {mapActions, mapMutations, mapGetters} from 'vuex';
export default {
  name: 'component-editor',
  data: function () {
    return {
      previousValue: '',
      language: '',
      options: [],
      size: '',
      field: {}
    };
  },
  mixins: [UrduMixin],
  computed: {
    show: {
      get () {
        let show = this.$store.getters['doctorStore/doctorsForm/showComponentEditor'];
        if (show) {
          this.formField = this.getFormField();
          return this.$store.getters['doctorStore/doctorsForm/showComponentEditor'];
        }
      },
      set (val) {}
    },
    formField: {
      get () {
        return this.field;
      },
      set (val) {
        this.field = val;
      }
    },
    getSelectOptions: {
      get () {
        return this.$store.getters['doctorStore/doctorsForm/doctorFormField/getSelectOptions'];
      },
      set (val) {}
    },
    propertiesSize: {
      get () {
        return (_.isNil(this.getFormField().properties) || _.isNil(this.getFormField().properties.size)) ? '' : this.getFormField().properties.size;
      },
      set (val) {
        this.editFieldProperty({
          property: 'size',
          val: val
        });
      }
    },
    propertiesDictionaryName: {
      get () {
        return (_.isNil(this.getFormField().properties) || _.isNil(this.getFormField().properties.dictionary_name)) ? '' : this.getFormField().properties.dictionary_name;
      },
      set (val) {
        this.editFieldProperty({
          property: 'dictionary_name',
          val: val
        });
      }
    },
    editorHeading: {
      get () {
        return this.getEditorHeading();
      },
      set (val) {}
    },
    inline: {
      get () {
        if (this.getInline()) {
          return 'true';
        } else {
          return 'false';
        }
      },
      set (val) {
        if (val === 'true') {
          this.changeInline(true);
        } else {
          this.changeInline(false);
        }
      }
    }
  },
  methods: {
    ...mapGetters({
      getEditorHeading: 'doctorStore/doctorsForm/getEditorHeading',
      getFormField: 'doctorStore/doctorsForm/doctorFormField/getFormField',
      getSelectOptions: 'doctorStore/doctorsForm/doctorFormField/getSelectOptions',
      getInline: 'doctorStore/doctorsForm/doctorFormField/getInline',
      showComponentEditor: 'doctorStore/doctorsForm/showComponentEditor'
    }),
    ...mapMutations({
      setShowComponentEditor: 'doctorStore/doctorsForm/setShowComponentEditor'
    }),
    ...mapActions({
      setEditForFormField: 'doctorStore/doctorsForm/doctorFormField/setEditForFormField',
      editFormComponent: 'doctorStore/doctorsForm/doctorFormField/editFormComponent',
      editFieldProperty: 'doctorStore/doctorsForm/doctorFormField/editFieldProperty',
      changeLanguage: 'doctorStore/doctorsForm/doctorFormField/changeLanguage',
      changeInline: 'doctorStore/doctorsForm/doctorFormField/changeInline',
      addProperty: 'doctorStore/doctorsForm/doctorFormField/addProperty',
      editProperty: 'doctorStore/doctorsForm/doctorFormField/editProperty',
      removeProperty: 'doctorStore/doctorsForm/doctorFormField/removeProperty'
    }),
    closeComponentEditor () {
      this.setShowComponentEditor({show: false});
    },
    editComponent (obj) {
      this.editFormComponent(obj);
    },
    editComponentUrdu (obj) {
      this.updateUrduValue(obj);
      this.editFormComponent({label_urdu: obj.value});
    },
    editLanguage (obj) {
      this.changeLanguage(obj).then(() => {
        this.language = obj;
      });
    }
  }
};
