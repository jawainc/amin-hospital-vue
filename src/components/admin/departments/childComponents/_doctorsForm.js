import {mapGetters, mapActions} from 'vuex';
import ComponentSelector from './componentSelector.vue';
import ComponentEditor from './componentEditor.vue';
import TextField from './formElements/textField.vue';
import TextArea from './formElements/textArea.vue';
import Select from './formElements/select.vue';
import CheckBox from './formElements/checkBox.vue';
import Table from './formElements/tableField.vue';
import SectionField from './formElements/section.vue';
export default {
  name: 'doctors-form',
  data: function () {
    return {
      dataLoading: false,
      showComponentSelector: false,
      editorHeading: ''
    };
  },
  components: {
    componentSelector: ComponentSelector,
    componentEditor: ComponentEditor,
    'text-field': TextField,
    'text-area': TextArea,
    'select-field': Select,
    'checkbox-field': CheckBox,
    'table-field': Table,
    sectionField: SectionField
  },
  computed: {
    ...mapGetters({
      doctorID: 'doctorStore/getDoctorID',
      doctorForm: 'doctorStore/doctorsForm/getDoctorForm'
    }),
    enableDictinary: {
      get () {
        if (this.$store.getters.getElement.enableDictinary) {
          return this.$store.getters.getElement.enableDictinary;
        } else {
          return false;
        }
      },
      set (value) {
        this.$store.commit('setElement', {
          key: 'enableDictinary',
          value: value
        });
      }
    }
  },
  methods: {
    ...mapActions({
      addCell: 'doctorStore/doctorsForm/addCell',
      removeCell: 'doctorStore/doctorsForm/removeCell',
      removeRow: 'doctorStore/doctorsForm/removeRow',
      changeCellLength: 'doctorStore/doctorsForm/changeCellLength',
      addFormFields: 'doctorStore/doctorsForm/addFormFields',
      changeCellProperties: 'doctorStore/doctorsForm/changeCellProperties'
    }),
    setElement (key, val) {
      this.$store.commit('setElement', {
        key: key,
        value: val
      });
    },
    displayFormFields (args) {
      this.addFormFields(args);
      this.showComponentSelector = true;
    },
    addComponent () {
      this.$store.commit('doctorStore/doctorsForm/addComponent', this.doctorID);
    },
    close () {
      this.$emit('closeDoctorsForm');
    }
  }
};
