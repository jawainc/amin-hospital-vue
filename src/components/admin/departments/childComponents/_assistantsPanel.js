import {db} from '@/firebase/db';
export default {
  name: 'doctors-assistants-panel',
  data: function () {
    return {
      dataLoading: false,
      items: [],
      selected: []
    };
  },
  mounted: function () {
    this.loadData();
  },
  methods: {
    addSelectedItems () {
      if (this.selected.length > 0) {
        this.$store.dispatch('doctorStore/assistants/setSelectedAssistants', this.selected);
      }
    },
    loadData () {
      this.dataLoading = true;
      db.collection('employees').get().then((docs) => {
        let temp = [];
        docs.forEach((doc) => {
          temp.push({
            id: doc.id,
            name: doc.data().name,
            fee: 0,
            percentage: 0
          });
        });
        this.items = temp;
      })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.dataLoading = false;
        });
    },
    handleSelectionChange (val) {
      this.selected = val;
    },
    handleCradHeaderCommand (command) {
      if (command === 'addSelected') {
        this.addSelectedItems();
        this.$emit('closeAssistantsPanel');
      }
      if (command === 'close') {
        this.$emit('closeAssistantsPanel');
      }
    },
    // ToDo add search functionality
    search (val) {
      console.log(val);
    }
  }
};
