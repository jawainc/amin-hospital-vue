import {mapGetters, mapActions, mapMutations} from 'vuex';
export default {
  name: 'select-field',
  data: function () {
    return {
      dummy: []
    };
  },
  props: ['data'],
  methods: {
    ...mapGetters({
      getIsAssistantForm: 'doctorStore/getIsAssistantForm'
    }),
    ...mapMutations({
      setShowComponentEditor: 'doctorStore/doctorsForm/setShowComponentEditor',
      setAssistantShowComponentEditor: 'doctorStore/assistantForm/setShowComponentEditor'
    }),
    ...mapActions({
      setEditForFormField: 'doctorStore/doctorsForm/doctorFormField/setEditForFormField',
      setAssistantEditForFormField: 'doctorStore/assistantForm/assistantFormField/setEditForFormField'
    }),
    editFormComponent () {
      if (this.getIsAssistantForm()) {
        this.callAssistantEditFormComponent();
      } else {
        this.callEditFormComponent();
      }
    },
    callEditFormComponent () {
      this.setEditForFormField({
        rowIndex: this.data.rowIndex,
        cellIndex: this.data.cellIndex,
        fieldIndex: this.data.fieldIndex,
        sectionFieldIndex: this.data.sectionFieldIndex,
        editorHeading: 'SelectField'
      }).then(() => {
        this.$nextTick(function () {
          this.setShowComponentEditor({show: true});
        });
      });
    },
    callAssistantEditFormComponent () {
      this.setAssistantEditForFormField({
        rowIndex: this.data.rowIndex,
        cellIndex: this.data.cellIndex,
        fieldIndex: this.data.fieldIndex,
        sectionFieldIndex: this.data.sectionFieldIndex,
        editorHeading: 'SelectField'
      }).then(() => {
        this.$nextTick(function () {
          this.setAssistantShowComponentEditor({show: true});
        });
      });
    }
  }
};
