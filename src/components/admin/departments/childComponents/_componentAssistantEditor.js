import _ from 'lodash';
import UrduMixin from '@/mixin/makeUrdu';
import {mapActions, mapMutations, mapGetters} from 'vuex';
export default {
  name: 'component-editor',
  data: function () {
    return {
      previousValue: '',
      language: '',
      options: [],
      size: '',
      field: {}
    };
  },
  mixins: [UrduMixin],
  computed: {
    show: {
      get () {
        let show = this.$store.getters['doctorStore/assistantForm/showComponentEditor'];
        if (show) {
          this.formField = this.getFormField();
          return this.$store.getters['doctorStore/assistantForm/showComponentEditor'];
        }
      },
      set (val) {}
    },
    formField: {
      get () {
        return this.field;
      },
      set (val) {
        this.field = val;
      }
    },
    getSelectOptions: {
      get () {
        return this.$store.getters['doctorStore/assistantForm/assistantFormField/getSelectOptions'];
      },
      set (val) {}
    },
    propertiesSize: {
      get () {
        return (_.isNil(this.getFormField().properties) || _.isNil(this.getFormField().properties.size)) ? '' : this.getFormField().properties.size;
      },
      set (val) {
        this.editFieldProperty({
          property: 'size',
          val: val
        });
      }
    },
    propertiesDictionaryName: {
      get () {
        return (_.isNil(this.getFormField().properties) || _.isNil(this.getFormField().properties.dictionary_name)) ? '' : this.getFormField().properties.dictionary_name;
      },
      set (val) {
        this.editFieldProperty({
          property: 'dictionary_name',
          val: val
        });
      }
    },
    editorHeading: {
      get () {
        return this.getEditorHeading();
      },
      set (val) {}
    },
    inline: {
      get () {
        if (this.getInline()) {
          return 'true';
        } else {
          return 'false';
        }
      },
      set (val) {
        if (val === 'true') {
          this.changeInline(true);
        } else {
          this.changeInline(false);
        }
      }
    }
  },
  methods: {
    ...mapGetters({
      getEditorHeading: 'doctorStore/assistantForm/getEditorHeading',
      getFormField: 'doctorStore/assistantForm/assistantFormField/getFormField',
      getSelectOptions: 'doctorStore/assistantForm/assistantFormField/getSelectOptions',
      getInline: 'doctorStore/assistantForm/assistantFormField/getInline',
      showComponentEditor: 'doctorStore/assistantFormField/showComponentEditor'
    }),
    ...mapMutations({
      setShowComponentEditor: 'doctorStore/assistantForm/setShowComponentEditor'
    }),
    ...mapActions({
      setEditForFormField: 'doctorStore/assistantForm/assistantFormField/setEditForFormField',
      editFormComponent: 'doctorStore/assistantForm/assistantFormField/editFormComponent',
      editFieldProperty: 'doctorStore/assistantForm/assistantFormField/editFieldProperty',
      changeLanguage: 'doctorStore/assistantForm/assistantFormField/changeLanguage',
      changeInline: 'doctorStore/assistantForm/assistantFormField/changeInline',
      addProperty: 'doctorStore/assistantForm/assistantFormField/addProperty',
      editProperty: 'doctorStore/assistantForm/assistantFormField/editProperty',
      removeProperty: 'doctorStore/assistantForm/assistantFormField/removeProperty'
    }),
    closeComponentEditor () {
      this.setShowComponentEditor({show: false});
    },
    editComponent (obj) {
      this.editFormComponent(obj);
    },
    editComponentUrdu (obj) {
      this.updateUrduValue(obj);
      this.editFormComponent({label_urdu: obj.value});
    },
    editLanguage (obj) {
      this.changeLanguage(obj).then(() => {
        this.language = obj;
      });
    }
  }
};
