import Doctors from './childComponents/doctors.vue';
import DoctorsPanel from './childComponents/doctorsPanel.vue';
import AssistantsPanel from './childComponents/assistantsPanel.vue';
import DoctorsForm from './childComponents/doctorsForm.vue';
import AssistantForm from './childComponents/assistantForm.vue';
import {mapGetters, mapMutations} from 'vuex';

import {db} from '@/firebase/db';
export default{
  name: 'admin-department-new',
  data: function () {
    return {
      form: {
        name: null
      },
      formSubmitting: false,
      isDoctorsPanel: false,
      isAssistantsPanel: false,
      isDoctorsForm: false,
      isAssistantForm: false,
      rules: {
        name: [
          {required: true, message: 'Department name is required', trigger: 'blur'}
        ]
      }
    };
  },
  components: {
    doctors: Doctors,
    doctorsPanel: DoctorsPanel,
    assistantsPanel: AssistantsPanel,
    doctorsForm: DoctorsForm,
    assistantForm: AssistantForm
  },
  computed: {
    ...mapGetters({
      totalPercentageError: 'doctorStore/totalPercentageError',
      feeError: 'doctorStore/feeError'
    }),
    showDoctorsPanel: {
      get: function () {
        return this.isDoctorsPanel;
      },
      set: function (val) {
        if (val === true) {
          this.isDoctorsPanel = true;
          this.isAssistantsPanel = false;
          this.isDoctorsForm = false;
          this.isAssistantForm = false;
        } else {
          this.isDoctorsPanel = false;
        }
      }
    },
    showAssistantsPanel: {
      get: function () {
        return this.isAssistantsPanel;
      },
      set: function (val) {
        if (val === true) {
          this.isDoctorsPanel = false;
          this.isDoctorsForm = false;
          this.isAssistantsPanel = true;
          this.isAssistantForm = false;
        } else {
          this.isAssistantsPanel = false;
        }
      }
    },
    showDoctorsForm: {
      get: function () {
        return this.isDoctorsForm;
      },
      set: function (val) {
        if (val === true) {
          this.isDoctorsPanel = false;
          this.isAssistantsPanel = false;
          this.isAssistantForm = false;
          this.isDoctorsForm = true;
        } else {
          this.isDoctorsForm = false;
        }
      }
    },
    showAssistantForm: {
      get: function () {
        return this.isAssistantForm;
      },
      set: function (val) {
        if (val === true) {
          this.isDoctorsPanel = false;
          this.isAssistantsPanel = false;
          this.isDoctorsForm = false;
          this.isAssistantForm = true;
        } else {
          this.isAssistantForm = false;
        }
      }
    }
  },
  mounted: function () {
    this.resetFields();
  },
  methods: {
    ...mapGetters({
      getSelectedDoctors: 'doctorStore/doctors/getSelectedDoctors',
      getDoctorsForSave: 'doctorStore/getDoctorsForSave'
    }),
    ...mapMutations({
      resetDoctors: 'doctorStore/doctors/reset',
      resetDoctorForm: 'doctorStore/doctorsForm/reset',
      resetAssistantForm: 'doctorStore/assistantForm/reset'
    }),
    submitForm (name) {
      if (!this.formSubmitting && !this.totalPercentageError.error && !this.feeError.error) {
        this.$refs[name].validate((valid) => {
          if (valid) {
            this.submit();
          } else {
            return false;
          }
        });
      }
    },
    submit () {
      this.formSubmitting = true;
      let data = {
        name: this.form.name,
        doctors: this.getDoctorsForSave()
      };
      let batch = db.batch();
      let depRef = db.collection('departments').doc(data.name);
      batch.set(depRef, {name: data.name});

      data.doctors.forEach((doctor) => {
        let docRef = db.collection('department_doctors').doc(`${data.name}-${doctor.id}`);
        batch.set(docRef, {
          department: {
            id: data.name,
            name: data.name
          },
          name: doctor.name,
          fee: doctor.fee,
          percentage: doctor.percentage,
          id: doctor.id,
          doctor_form: doctor.doctor_form,
          assistants: doctor.assistants,
          assistant_form: {
            is_form: doctor.assistant_form.is_form,
            form: doctor.assistant_form.form
          }
        });
      });
      batch.commit()
        .then(() => {
          this.showSaveSuccessMessage();
          this.resetForm();
        })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.formSubmitting = false;
        });
    },
    handleCradHeaderCommand (command) {
      if (command === 'showDoctorPanel') {
        this.showDoctorsPanel = true;
      }
    },
    resetFields () {
      this.form.name = null;
      this.resetDoctors();
      this.resetDoctorForm();
      this.resetAssistantForm();
      this.formSubmitting = false;
      this.isDoctorsPanel = false;
      this.isAssistantsPanel = false;
      this.isDoctorsForm = false;
      this.isAssistantForm = false;
    },
    resetForm () {
      this.resetFields();
      this.$nextTick(function () {
        // DOM is now updated
        // `this` is bound to the current instance
        this.$refs['adminDepartmentForm'].resetFields();
      });
    }
  }
};
