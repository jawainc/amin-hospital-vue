import {db} from '@/firebase/db';

export default{
  name: 'admin-departments-list',
  data: function () {
    return {
      deleting: false,
      dataLoading: false,
      items: []
    };
  },
  mounted: function () {
    this.loadItems();
  },
  methods: {
    loadItems () {
      this.dataLoading = true;
      db.collection('departments').get().then((docs) => {
        this.items = [];
        docs.forEach((doc) => {
          this.items.push({
            id: doc.id,
            name: doc.data().name
          });
        });
      })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.dataLoading = false;
        });
    },
    handleEdit (row) {
      this.$router.push({path: '/admin/departments/edit/' + row.id});
    },
    handleDelete (index, row) {
      this.$confirm(`This will delete department "${row.name}". Continue?`, 'Warning', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        type: 'warning'
      }).then(() => {
        db.collection('departments').doc(row.id).delete()
          .then(() => {
            this.showDeleteMessage();
            this.items.splice(index, 1);
          })
          .catch((error) => {
            this.showErrorMessage(error);
          })
          .then(() => {
            this.deleting = false;
          });
      });
    }
  }
};
