import Balance from './balance';
import {db} from '@/firebase/db';
import moment from 'moment';
export default {
  data () {
    return {
      loading: false,
      activeContainer: 'Balance',
      balance: false,
      add_payment: false,
      add_expanse: false,
      dateRange: [],
      accountBalance: {
        income: 0,
        expanse: 0,
        amountBalance: 0
      }
    };
  },
  components: {
    balance: Balance
  },
  mounted: function () {
    this.balance = true;
    this.loadBalance();
  },
  methods: {
    change (label) {
      switch (label) {
        case 'Balance':
          this.dateRange = [];
          this.loadBalance();
          break;
        case 'Add Payment':
          this.loadPayment();
          break;
        case 'Add Expanse':
          this.loadExpanse();
          break;
        default:
          this.loadBalance();
      }
    },
    async loadBalance () {
      this.loading = true;
      let mStart = moment().local().startOf('day').valueOf();
      let mEnd = moment().local().endOf('day').valueOf();
      let start = new Date(mStart);
      let end = new Date(mEnd);
      try {
        let incomeResult = await db.collection('incomes')
          .where('created_at', '>=', start)
          .where('created_at', '<=', end)
          .get()
          .then((result) => {
            let incomes = 0;
            result.forEach((income) => {
              incomes += income.data().amount;
            });
            return incomes;
          });
        let expenseResult = await db.collection('expenses')
          .where('created_at', '>=', start)
          .where('created_at', '<=', end)
          .get()
          .then((result) => {
            let expenses = 0;
            result.forEach((expense) => {
              expenses += expense.data().amount;
            });
            return expenses;
          });

        this.accountBalance.income = incomeResult;
        this.accountBalance.expanse = expenseResult;
        this.accountBalance.amountBalance = incomeResult - expenseResult;
      } catch (e) {
        this.showErrorMessage(e);
      } finally {
        this.loading = false;
      }
    },
    async dateChange (value) {
      this.loading = true;
      let mStart = moment(value[0]).local().startOf('day').valueOf();
      let mEnd = moment(value[1]).local().endOf('day').valueOf();
      let start = new Date(mStart);
      let end = new Date(mEnd);
      try {
        let incomeResult = await db.collection('incomes')
          .where('created_at', '>=', start)
          .where('created_at', '<=', end)
          .get()
          .then((result) => {
            let incomes = 0;
            result.forEach((income) => {
              incomes += income.data().amount;
            });
            return incomes;
          });
        let expenseResult = await db.collection('expenses')
          .where('created_at', '>=', start)
          .where('created_at', '<=', end)
          .get()
          .then((result) => {
            let expenses = 0;
            result.forEach((expense) => {
              expenses += expense.data().amount;
            });
            return expenses;
          });

        this.accountBalance.income = incomeResult;
        this.accountBalance.expanse = expenseResult;
        this.accountBalance.amountBalance = incomeResult - expenseResult;
      } catch (e) {
        this.showErrorMessage(e);
      } finally {
        this.loading = false;
      }
    }
  }
};
