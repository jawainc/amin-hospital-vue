import {db} from '@/firebase/db';
import moment from 'moment';
export default {
  data () {
    return {
      dataLoading: false,
      formSubmitting: false,
      search: {
        department: null,
        dateRange: []
      },
      dateRange: [],
      searchData: [],
      expanseData: [],
      departments: [],
      items: [],
      rules: {
        department: [
          {required: true, message: 'Department is required'}
        ],
        dateRange: [
          {required: true, message: 'Date range is required'}
        ]
      }
    };
  },
  filters: {
    moment: function (date) {
      return moment.unix(date.seconds).format('DD/MM/YYYY');
    }
  },
  mounted: function () {
    this.loadData();
  },
  firestore () {
    return {
      expenses: db.collection('expenses'),
      departments: db.collection('departments')
    };
  },
  methods: {
    loadData () {
      this.dataLoading = true;
      this.$firestore.departments.get().then((docs) => {
        let items = [];
        docs.forEach((doc) => {
          items.push({
            value: doc.id,
            label: doc.data().name
          });
        });
        this.departments = items;
      })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.dataLoading = false;
        });
    },
    doSearchData () {
      if (!this.formSubmitting) {
        this.$refs['searchForm'].validate((valid) => {
          if (valid) {
            this.fetchData();
          } else {
            return false;
          }
        });
      }
    },
    fetchData () {
      this.searchData = [];
      this.dataLoading = true;
      this.formSubmitting = true;
      let mStart = moment(this.search.dateRange[0]).local().startOf('day').valueOf();
      let mEnd = moment(this.search.dateRange[1]).local().endOf('day').valueOf();
      let start = new Date(mStart);
      let end = new Date(mEnd);
      this.$firestore.expenses
        .where('department.id', '==', this.search.department)
        .where('created_at', '>=', start)
        .where('created_at', '<=', end)
        .get()
        .then((result) => {
          console.log(result);
          result.forEach((share) => {
            this.expanseData.push(share.data());
          });
        })
        .catch((e) => {
          console.log(e);
          this.showErrorMessage(e.message);
        })
        .then(() => {
          this.dataLoading = false;
          this.formSubmitting = false;
        });
    },
    getSummaries (param) {
      const { columns, data } = param;
      const sums = [];
      columns.forEach((column, index) => {
        if (index === 0) {
          sums[index] = 'Total';
          return;
        }
        if (index === 2) {
          sums[index] = '';
          return;
        }
        const values = data.map(item => Number(item[column.property]));

        if (!values.every(value => isNaN(value))) {
          sums[index] = 'Rs: ' + values.reduce((prev, curr) => {
            const value = Number(curr);
            if (!isNaN(value)) {
              return prev + curr;
            } else {
              return prev;
            }
          }, 0);
        } else {
          sums[index] = 'N/A';
        }
      });

      return sums;
    }
  }
};
