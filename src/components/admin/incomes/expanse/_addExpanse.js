import {db} from '@/firebase/db';
export default {
  data: function () {
    return {
      dataLoading: false,
      formSubmitting: false,
      departments: [],
      form: {
        amount: null,
        department: null,
        details: null,
        createdAt: null
      },
      formName: 'adminExpanseAddForm',
      rules: {
        amount: [
          {required: true, message: 'Amount is required', trigger: 'blur'},
          {validator: this.positiveNumber, trigger: 'blur'}
        ]
      }
    };
  },
  firestore () {
    return {
      expenses: db.collection('department_shares'),
      departments: db.collection('departments')
    };
  },
  mounted: function () {
    this.dataLoading = true;
    this.$firestore.departments.get().then((docs) => {
      let items = [];
      docs.forEach((doc) => {
        items.push({
          value: doc.id,
          label: doc.data().name
        });
      });
      this.departments = items;
    })
      .catch((error) => {
        this.showErrorMessage(error);
      })
      .then(() => {
        this.dataLoading = false;
      });
  },
  methods: {
    submitForm () {
      if (!this.formSubmitting) {
        this.$refs[this.formName].validate((valid) => {
          if (valid) {
            this.save();
          } else {
            return false;
          }
        });
      }
    },
    save () {
      this.formSubmitting = true;
      let data = {
        amount: parseInt(this.form.amount),
        department: {
          id: this.form.department.value,
          name: this.form.department.label
        },
        created_at: new Date(Date.parse(this.form.createdAt)),
        details: this.form.details
      };
      db.collection('expenses').add(data).then((ref) => {
        this.showSaveSuccessMessage();
        this.$refs[this.formName].resetFields();
        this.resetFields();
      })
        .catch((e) => {
          this.showErrorMessage(e);
        })
        .then(() => {
          this.formSubmitting = false;
        });
    },
    resetFields () {
      this.form.amount = null;
      this.form.createdAt = null;
      this.form.details = null;
    }
  }
};
