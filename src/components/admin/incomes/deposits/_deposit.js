import {db} from '@/firebase/db';
import moment from 'moment';
export default {
  data () {
    return {
      loading: false,
      dataLoading: false,
      formSubmitting: false,
      postData: {},
      search: {
        department: null,
        dateRange: []
      },
      incomeData: [],
      searchData: [],
      departments: [],
      items: [],
      rules: {
        department: [
          {required: true, message: 'Department is required'}
        ],
        dateRange: [
          {required: true, message: 'Date range is required'}
        ]
      }
    };
  },
  filters: {
    moment: function (date) {
      return moment.unix(date.seconds).format('DD/MM/YYYY');
    }
  },
  mounted: function () {
    this.loadData();
  },
  methods: {
    loadData () {
      db.collection('departments').get().then((docs) => {
        let items = [];
        docs.forEach((doc) => {
          items.push({
            value: doc.id,
            label: doc.data().name
          });
        });
        this.departments = items;
      })
        .catch((error) => {
          this.showErrorMessage(error);
        });
    },
    doSearchData () {
      if (!this.formSubmitting) {
        this.$refs['searchForm'].validate((valid) => {
          if (valid) {
            this.fetchData();
          } else {
            return false;
          }
        });
      }
    },
    fetchData () {
      this.incomeData = [];
      this.loading = true;
      this.formSubmitting = true;
      let mStart = moment(this.search.dateRange[0]).local().startOf('day').valueOf();
      let mEnd = moment(this.search.dateRange[1]).local().endOf('day').valueOf();
      let start = new Date(mStart);
      let end = new Date(mEnd);
      db.collection('patient_entries')
        .where('department.id', '==', this.search.department)
        .where('created_at', '>=', start)
        .where('created_at', '<=', end)
        .get()
        .then((result) => {
          result.forEach((income) => {
            this.incomeData.push(income.data());
          });
        })
        .catch((e) => {
          console.log(e);
          this.showErrorMessage(e);
        })
        .then(() => {
          this.loading = false;
          this.loading = false;
          this.formSubmitting = false;
        });
    },
    getSummaries (param) {
      const { columns, data } = param;
      const sums = [];
      columns.forEach((column, index) => {
        if (index === 0) {
          sums[index] = 'Total';
          return;
        }
        if (index === 1) {
          sums[index] = '';
          return;
        }
        if (index === 2) {
          sums[index] = '';
          return;
        }
        if (index === 3) {
          sums[index] = '';
          return;
        }
        if (index === 6) {
          sums[index] = '';
          return;
        }

        let values = null;
        if (index === 4) {
          values = data.map(item => {
            return Number(item.discount.amount);
          });
        } else {
          values = data.map(item => {
            return Number(item[column.property]);
          });
        }
        if (!values.every(value => isNaN(value))) {
          sums[index] = 'Rs: ' + values.reduce((prev, curr) => {
            const value = Number(curr);
            if (!isNaN(value)) {
              return prev + curr;
            } else {
              return prev;
            }
          }, 0);
        } else {
          sums[index] = 'N/A';
        }
      });

      return sums;
    }
  }
};
