import {db} from '@/firebase/db';
import moment from 'moment';
export default {
  data () {
    return {
      dataLoading: false,
      formSubmitting: false,
      search: {
        doctor: null,
        dateRange: []
      },
      dateRange: [],
      searchData: [],
      doctors: [],
      items: [],
      rules: {
        doctor: [
          {required: true, message: 'Doctor is required'}
        ],
        dateRange: [
          {required: true, message: 'Date range is required'}
        ]
      }
    };
  },
  filters: {
    moment: function (date) {
      return moment.unix(date.seconds).format('DD/MM/YYYY');
    }
  },
  mounted: function () {
    this.loadData();
  },
  firestore () {
    return {
      shares: db.collection('doctor_shares'),
      doctors: db.collection('doctors')
    };
  },
  methods: {
    loadData () {
      this.dataLoading = true;
      this.$firestore.doctors.get().then((docs) => {
        let items = [];
        docs.forEach((doc) => {
          items.push({
            value: doc.id,
            label: doc.data().name
          });
        });
        this.doctors = items;
      })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.dataLoading = false;
        });
    },
    doSearchData () {
      if (!this.formSubmitting) {
        this.$refs['searchForm'].validate((valid) => {
          if (valid) {
            this.fetchData();
          } else {
            return false;
          }
        });
      }
    },
    fetchData () {
      this.searchData = [];
      this.loading = true;
      this.formSubmitting = true;
      let mStart = moment(this.search.dateRange[0]).local().startOf('day').valueOf();
      let mEnd = moment(this.search.dateRange[1]).local().endOf('day').valueOf();
      let start = new Date(mStart);
      let end = new Date(mEnd);
      this.$firestore.shares
        .where('doctor_id', '==', this.search.doctor)
        .where('created_at', '>=', start)
        .where('created_at', '<=', end)
        .get()
        .then((result) => {
          result.forEach((share) => {
            this.searchData.push(share.data());
          });
        })
        .catch((e) => {
          console.log(e);
          this.showErrorMessage(e.message);
        })
        .then(() => {
          this.loading = false;
          this.formSubmitting = false;
        });
    },
    getSummaries (param) {
      const { columns, data } = param;
      const sums = [];
      columns.forEach((column, index) => {
        if (index === 0) {
          sums[index] = 'Total';
          return;
        }
        let values = null;
        if (index === 2) {
          values = data.map(item => {
            return Number(item.amount);
          });
        } else {
          values = data.map(item => {
            return Number(item[column.property]);
          });
        }
        if (!values.every(value => isNaN(value))) {
          sums[index] = 'Rs: ' + values.reduce((prev, curr) => {
            const value = Number(curr);
            if (!isNaN(value)) {
              return prev + curr;
            } else {
              return prev;
            }
          }, 0);
        } else {
          sums[index] = 'N/A';
        }
      });

      return sums;
    }
  }
};
