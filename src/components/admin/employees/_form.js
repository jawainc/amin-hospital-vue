import {db} from '@/firebase/db';
import moment from 'moment';

export default{
  name: 'admin-doctors-new',
  data: function () {
    return {
      form: {
        name: null,
        email: null,
        nic: null,
        phone: null,
        gender: null,
        address: null,
        salary: null,
        dailyBases: null,
        joiningDate: null,
        notes: null
      },
      formName: 'adminEmployeeForm',
      rules: {
        name: [
          {required: true, message: 'Name is required', trigger: 'blur'}
        ],
        email: [
          {type: 'email', message: 'Incorrect email address', trigger: 'blur'}
        ],
        salary: [
          {validator: this.positiveNumber, trigger: 'blur'}
        ],
        dailyBases: [
          {validator: this.positiveNumber, trigger: 'blur'}
        ]
      },
      formSubmitting: false,
      button: 'Create'
    };
  },
  props: ['edit', 'editId'],
  mounted: function () {
    if (this.edit === true) {
      this.button = 'Update';
      this.loadData();
    }
  },
  methods: {
    submitForm () {
      if (!this.formSubmitting) {
        this.$refs[this.formName].validate((valid) => {
          if (valid) {
            if (this.edit === true) {
              this.update();
            } else {
              this.save();
            }
          } else {
            return false;
          }
        });
      }
    },
    save () {
      this.formSubmitting = true;
      this.$emit('formSubmitting', true);
      let data = {
        name: this.form.name,
        email: this.form.email,
        nic: this.form.nic,
        phone: this.form.phone,
        gender: this.form.gender,
        address: this.form.address,
        salary: parseInt(this.form.salary),
        dailyBases: parseInt(this.form.dailyBases),
        joiningDate: this.form.joiningDate,
        notes: this.form.notes
      };
      db.collection('employees').add(data)
        .then((docRef) => {
          this.showSaveSuccessMessage();
        })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.resetForm();
          this.formSubmitting = false;
        });
    },
    update () {
      this.formSubmitting = true;
      this.$emit('formSubmitting', true);
      let data = {
        name: this.form.name,
        email: this.form.email,
        nic: this.form.nic,
        phone: this.form.phone,
        gender: this.form.gender,
        address: this.form.address,
        salary: parseInt(this.form.salary),
        dailyBases: parseInt(this.form.dailyBases),
        joiningDate: this.form.joiningDate,
        notes: this.form.notes
      };
      db.collection('employees').doc(this.editId).update(data)
        .then(() => this.showSuccessMessage('Updated Successfully'))
        .catch((error) => this.showErrorMessage(error))
        .then(() => { this.formSubmitting = false; });
    },
    loadData () {
      this.$emit('loadingData', true);
      db.collection('employees').doc(this.editId).get()
        .then((doc) => {
          if (doc.exists) {
            let result = doc.data();
            this.form.name = result.name;
            this.form.email = result.email;
            this.form.nic = result.nic;
            this.form.phone = result.phone;
            this.form.gender = result.gender;
            this.form.address = result.address;
            this.form.percentage = result.percentage;
            this.form.salary = result.salary;
            this.form.dailyBases = result.dailyBases;
            this.form.joiningDate = moment.unix(result.joiningDate.seconds).toDate();
            this.form.notes = result.notes;
          }
        })
        .catch((error) => this.showErrorMessage(error))
        .then(() => this.$emit('loadingData', false));
    },
    resetForm () {
      this.form.name = null;
      this.form.email = null;
      this.form.nic = null;
      this.form.phone = null;
      this.form.gender = null;
      this.form.address = null;
      this.form.percentage = null;
      this.form.salary = null;
      this.form.dailyBases = null;
      this.form.joiningDate = null;
      this.form.notes = null;
    }
  }
};
