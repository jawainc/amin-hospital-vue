import {db} from '@/firebase/db';

export default{
  name: 'admin-doctors-list',
  data: function () {
    return {
      dataLoading: false,
      deleting: false,
      items: []
    };
  },
  mounted: function () {
    this.load();
  },
  methods: {
    load () {
      this.dataLoading = true;
      db.collection('employees').get().then((docs) => {
        this.items = [];
        docs.forEach((doc) => {
          this.items.push({
            id: doc.id,
            name: doc.data().name
          });
        });
      })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.dataLoading = false;
        });
    },
    handleEdit (row) {
      this.$router.push({path: '/admin/employees/edit/' + row.id});
    },
    handleDelete (index, row) {
      this.$confirm(`This will delete employee "${row.name}". Continue?`, 'Warning', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        type: 'warning'
      }).then(() => {
        this.deleting = true;
        db.collection('employees').doc(row.id).delete()
          .then(() => {
            this.showDeleteMessage();
            this.items.splice(index, 1);
          })
          .catch((error) => {
            this.showErrorMessage(error);
          })
          .then(() => {
            this.deleting = false;
          });
      });
    }
  }
};
