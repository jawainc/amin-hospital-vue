import Form from './form';
export default{
  name: 'admin-doctors-edit',
  props: ['id'],
  data: function () {
    return {
      formSubmitting: false
    };
  },
  components: {
    'edit-form': Form
  },
  methods: {
    formSubmit (val) {
      this.formSubmitting = val;
    },
    formLoading (val) {
      this.formSubmitting = val;
    }
  }
};
