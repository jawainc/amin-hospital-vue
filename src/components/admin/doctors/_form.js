import {db} from '@/firebase/db';

export default{
  name: 'admin-doctors-new',
  data: function () {
    return {
      form: {
        name: null,
        email: null,
        phone: null,
        gender: null,
        address: null
      },
      formSubmitting: false,
      formError: false,
      errorMessage: '',
      button: 'Create',
      formName: '',
      rules: {
        name: [
          {required: true, message: 'Name is required', trigger: 'blur'}
        ],
        email: [
          {type: 'email', message: 'Incorrect email', trigger: 'blur'}
        ]
      }
    };
  },
  props: ['apiEndPoint', 'edit', 'editId'],
  mounted: function () {
    if (this.edit === true) {
      this.button = 'Update';
      this.loadData();
    }
  },
  methods: {
    submitForm (name) {
      this.formName = name;
      if (!this.formSubmitting) {
        this.$refs[name].validate((valid) => {
          if (valid) {
            if (this.edit === true) {
              this.update();
            } else {
              this.save();
            }
          } else {
            return false;
          }
        });
      }
    },
    save () {
      this.formSubmitting = true;
      this.$emit('formSubmitting', true);
      let data = {
        name: this.form.name,
        email: this.form.email,
        phone: this.form.phone,
        gender: this.form.gender,
        address: this.form.address
      };
      db.collection('doctors').add(data)
        .then((docRef) => {
          this.showSaveSuccessMessage();
        })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.resetForm();
          this.formSubmitting = false;
        });
    },
    update () {
      this.formSubmitting = true;
      this.$emit('formSubmitting', true);
      let data = {
        id: this.editId,
        name: this.form.name,
        email: this.form.email,
        phone: this.form.phone,
        gender: this.form.gender,
        address: this.form.address
      };
      db.collection('doctors').doc(this.editId).update(data)
        .then(() => this.showSuccessMessage('Updated Successfully'))
        .catch((error) => this.showErrorMessage(error))
        .then(() => { this.formSubmitting = false; });
    },
    loadData () {
      db.collection('doctors').doc(this.editId).get()
        .then((doc) => {
          if (doc.exists) {
            let result = doc.data();
            this.form.name = result.name;
            this.form.email = result.email;
            this.form.phone = result.phone;
            this.form.gender = result.gender;
            this.form.address = result.address;
          }
        })
        .catch((error) => this.showErrorMessage(error));
    },
    resetForm () {
      this.form.name = null;
      this.form.email = null;
      this.form.phone = null;
      this.form.gender = null;
      this.form.address = null;
    }
  }
};
