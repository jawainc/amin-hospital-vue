import {db} from '@/firebase/db';
export default{
  name: 'admin-users-list',
  data: function () {
    return {
      deleting: false,
      loading: false,
      items: []
    };
  },
  mounted: function () {
    this.loadUsers();
  },
  methods: {
    loadUsers () {
      this.loading = true;
      db.collection('users').get()
        .then((users) => {
          this.items = [];
          users.forEach((user) => {
            this.items.push({
              id: user.id,
              data: user.data()
            });
          });
        })
        .catch((e) => {
          this.showErrorMessage(e.message);
        })
        .then(() => {
          this.loading = false;
        });
    },
    handleEdit (id) {
      this.$router.push({path: '/admin/users/edit/' + id});
    },
    handleDelete (index, row) {
      this.$confirm(`This will delete user "${row.data.login_id}". Continue?`, 'Warning', {
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        type: 'warning'
      }).then(() => {
        this.deleting = true;
        db.collection('users').doc(row.id).delete()
          .then(() => {
            this.showDeleteMessage();
            this.items.splice(index, 1);
          })
          .catch((error) => {
            this.showErrorMessage(error);
          })
          .then(() => {
            this.deleting = false;
          });
      });
    }
  }
};
