import Form from './form';
export default{
  name: 'admin-doctors-new',
  data: function () {
    return {
      formSubmitting: false
    };
  },
  components: {
    'new-form': Form
  },
  methods: {
    formSubmit (val) {
      this.formSubmitting = val;
    }
  }
};
