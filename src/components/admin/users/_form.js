import {db, fakeAuth} from '@/firebase/db';
export default{
  name: 'admin-users-form',
  props: ['edit', 'editId'],
  data: function () {
    return {
      form: {
        role: null,
        role_type: null,
        selectedDoctor: null,
        login_id: null,
        password: null
      },
      showOperator: false,
      operators: ['Reception', 'Lab', 'X-ray'],
      showDoctors: false,
      doctors: [],
      formSubmitting: false,
      button: 'Create',
      loading: false,
      dummy: null,
      rules: {
        login_id: [
          {required: true, message: 'Login id is required', trigger: 'blur'},
          {type: 'email', message: 'Incorrect email', trigger: 'blur'}
        ],
        password: [
          {required: true, validator: this.validatePass, trigger: 'blur'}
        ]
      }
    };
  },
  created: async function () {
    let docs = await this.loadDoctors();
    this.resetFields();
    if (docs && this.edit === true) {
      this.loading = true;
      this.button = 'Update';
      if (docs) {
        this.loadData();
      }
    }
  },
  methods: {
    submitForm (name) {
      if (!this.formSubmitting) {
        this.$refs[name].validate((valid) => {
          if (valid) {
            if (this.edit === true) {
              this.update();
            } else {
              this.submit();
            }
          } else {
            return false;
          }
        });
      }
    },
    submit () {
      this.formSubmitting = true;
      let data = {
        role: this.form.role,
        role_type: this.form.role_type,
        selected_doctor: this.form.selectedDoctor,
        login_id: this.form.login_id
      };
      fakeAuth.createUserWithEmailAndPassword(data.login_id, this.form.password)
        .then((result) => {
          fakeAuth.signOut();
          let uid = result.user.uid;
          db.collection('users').doc(uid).set({
            role: this.form.role,
            role_type: this.form.role_type,
            selected_doctor: this.form.selectedDoctor,
            login_id: result.user.email
          }).then((result) => {
            this.resetForm();
            this.showSaveSuccessMessage();
          });
        })
        .catch((error) => {
          let errorCode = error.code;
          let errorMessage = error.message;
          if (errorCode === 'auth/weak-password') {
            this.showErrorMessage('The password is too weak.');
          } else {
            this.showErrorMessage(errorMessage);
          }
        })
        .finally(() => {
          this.formSubmitting = false;
        });
    },
    update () {
      this.formSubmitting = true;
      db.collection('users').doc(this.editId).set({
        role: this.form.role,
        role_type: this.form.role_type,
        selected_doctor: this.form.selectedDoctor,
        login_id: this.form.login_id
      }).then((result) => {
        this.showSaveSuccessMessage();
      }).catch((e) => {
        this.showErrorMessage(e.message);
      })
        .then(() => {
          this.formSubmitting = false;
        });
    },
    loadDoctors () {
      return db.collection('department_doctors').get().then((docs) => {
        docs.forEach((doc) => {
          let doctor = doc.data();
          this.doctors.push({
            department_doctor_id: doc.id,
            name: doctor.name,
            department: doctor.department.name,
            department_id: doctor.department.id,
            doctor_id: doctor.id
          });
        });
        return Promise.resolve(true);
      })
        .catch((error) => {
          this.showErrorMessage(error);
          return Promise.resolve(false);
        });
    },
    doctorChange (index) {
      this.form.selectedDoctor = this.doctors[index];
    },
    roleChanged (val) {
      switch (val) {
        case 'Assistant':
          this.dummy = null;
          this.showDoctors = true;
          this.showOperator = false;
          this.role_type = 'Assistant';
          break;
        case 'Operator':
          this.dummy = null;
          this.showOperator = true;
          this.showDoctors = false;
          break;
        case 'Doctor':
          this.dummy = null;
          this.showDoctors = true;
          this.showOperator = false;
          this.role_type = null;
          break;
        case 'Admin':
          this.dummy = null;
          this.role_type = 'Admin';
          this.showOperator = false;
          this.showDoctors = false;
          break;
        default:
          this.dummy = null;
          this.showOperator = false;
          this.showDoctors = false;
          this.role_type = null;
      }
    },
    loadData () {
      this.loading = true;
      db.collection('users').doc(this.editId).get()
        .then((user) => {
          if (user.exists) {
            let data = user.data();
            this.form.role = data.role;
            this.form.role_type = data.role_type;
            this.form.login_id = data.login_id;
            if (this.form.role === 'Assistant' || this.form.role === 'Doctor') {
              this.form.selectedDoctor = data.selected_doctor;

              for (let i = 0; i < this.doctors.length; i++) {
                if (this.form.selectedDoctor.department_doctor_id === this.doctors[i].department_doctor_id) {
                  this.roleChanged(this.form.role);
                  this.dummy = i;
                  break;
                }
              }
            } else {
              this.roleChanged(this.form.role);
            }
          }
        })
        .catch((e) => {
          this.showErrorMessage(e.message);
        })
        .then(() => {
          this.loading = false;
        });
    },
    resetFields () {
      this.form.login_id = null;
      this.form.password = null;
      this.form.role = null;
      this.form.role_type = null;
      this.form.selectedDoctor = null;
      this.showOperator = false;
      this.showDoctors = false;
    },
    resetForm () {
      this.resetFields();
      this.$nextTick(function () {
        // DOM is now updated
        // `this` is bound to the current instance
        this.$refs['adminUserForm'].resetFields();
      });
    },
    validatePass (rule, value, callback) {
      if (this.edit === true) {
        callback();
      } else if (value && value.length > 0) {
        callback();
      } else {
        return callback(new Error('Password is required'));
      }
    }
  }
};
