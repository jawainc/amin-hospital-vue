import _ from 'lodash';
import {db} from '@/firebase/db';

export default {
  name: 'admin-search',
  data: function () {
    return {
      genderOptions: [
        {
          value: 'male',
          label: 'Male'
        },
        {
          value: 'female',
          label: 'Female'
        }
      ],
      searchData: [],
      searching: false,
      search: {
        phone: null,
        name: null,
        age: null,
        location: null,
        gender: null
      },
      edit: {},
      inEdit: {},
      inEditIndex: null,
      dialogEditFormVisible: false,
      loading: false
    };
  },
  methods: {
    searchPatients () {
      this.searching = true;
      let query = db.collection('patients');
      if (!_.isEmpty(this.search.phone)) {
        query = query.where('phone', '>=', this.search.phone)
          .where('phone', '<=', this.search.phone + '\uf8ff');
      } else if (!_.isEmpty(this.search.name) && _.isEmpty(this.search.age) && _.isEmpty(this.search.location) && !_.isEmpty(this.search.gender)) {
        query = query.where('name', '>=', this.search.name)
          .where('name', '<=', this.search.name + '\uf8ff');
      } else {
        if (!_.isEmpty(this.search.name)) {
          query = query.where('name', '>', this.search.name);
        }
        if (!_.isEmpty(this.search.age)) {
          query = query.where('age', '==', this.search.age);
        }
        if (!_.isEmpty(this.search.location)) {
          query = query.where('location', '==', this.search.location);
        }
        if (!_.isEmpty(this.search.gender)) {
          query = query.where('gender', '==', this.search.gender);
        }
      }
      query.get().then((results) => {
        this.searchData = [];
        if (!results.empty) {
          _.forEach(results.docs, (doc) => {
            let dt = doc.data();
            dt.patient_id = doc.id;
            this.searchData.push(dt);
          });
        } else {
          this.searchData = [];
        }
      })
        .catch((error) => {
          this.showErrorMessage(error);
        })
        .then(() => {
          this.searching = false;
        });
    },
    handleEdit (index) {
      this.inEditIndex = index;
      this.edit = _.clone(this.searchData[index]);
      this.inEdit = _.clone(this.searchData[index]);
      this.dialogEditFormVisible = true;
    },
    editPatient () {
      this.loading = true;
      if (this.edit.phone === this.inEdit.phone) {
        db.collection('patients').doc(this.edit.patient_id).update({
          name: this.edit.name,
          age: this.edit.age,
          location: this.edit.location,
          nic: this.edit.nic,
          gender: this.edit.gender
        })
          .then((ref) => {
            this.showSaveSuccessMessage();
            this.$set(this.searchData, this.inEditIndex, this.edit);
            this.inEditIndex = null;
          }).catch((e) => {
            console.log(e);
            this.showErrorMessage(e);
          })
          .then(() => {
            this.dialogEditFormVisible = false;
            this.loading = false;
          });
      } else {
        db.collection('phones').where('phone', '==', this.edit.phone).get()
          .then((result) => {
            if (result.empty) {
              let batch = db.batch();
              let phoneRef = db.collection('phones').doc(this.edit.phone_id);
              batch.set(phoneRef, {phone: this.edit.phone});
              let patientref = db.collection('patients').doc(this.edit.patient_id);
              batch.update(patientref, {
                phone: this.edit.phone,
                name: this.edit.name,
                age: this.edit.age,
                location: this.edit.location,
                nic: this.edit.nic,
                gender: this.edit.gender
              });
              batch.commit().then(() => {
                this.showSaveSuccessMessage();
                this.$set(this.searchData, this.inEditIndex, this.edit);
                this.inEditIndex = null;
              })
                .catch((error) => {
                  console.log(error);
                  this.showErrorMessage(error);
                })
                .then(() => {
                  this.dialogEditFormVisible = false;
                  this.loading = false;
                });
            } else {
              this.showErrorMessage('Phone Number Already Exists');
              this.loading = false;
            }
          });
      }
    },
    formReset () {
      this.search = {
        phone: null,
        name: null,
        age: null,
        location: null,
        gender: null
      };
    }
  }
};
