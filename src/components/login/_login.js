import {firebase} from '@/firebase/db';

export default {
  name: 'login',
  data: function () {
    return {
      form: {
        login_id: null,
        password: null
      },
      formSubmitting: false,
      formError: false,
      errorMessage: '',
      rules: {
        login_id: [
          {required: true, message: 'Login Id is required', trigger: 'blur'}
        ],
        password: [
          {required: true, message: 'Password is required', trigger: 'blur'}
        ]
      }
    };
  },
  methods: {
    auth: function (name) {
      this.$refs[name].validate((valid) => {
        if (valid) {
          this.submit();
        } else {
          return false;
        }
      });
    },
    submit: function () {
      this.formSubmitting = true;
      firebase.auth().signInWithEmailAndPassword(this.form.login_id, this.form.password)
        .then((response) => {
          firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
            .then(() => {
              // Existing and future Auth states are now persisted in the current
              // session only. Closing the window would clear any existing state even
              // if a user forgets to sign out.
              // console.log('persistent');
            });
        })
        .catch((error) => {
          if (error.code !== 'auth/unsupported-persistence-type') {
            this.showErrorMessage(error.message);
          }
          this.formSubmitting = false;
        });
    }
  }
};
