import Vue from 'vue';
import Router from 'vue-router';

// Login
const Login = () => import('@/components/login/Login');

// Reception
const Reception = () => import('@/components/reception/Reception');
const OutDoor = () => import('@/components/reception/outdoor/outdoor');
const Discount = () => import('@/components/reception/discount/patients');
const DiscountDetail = () => import('@/components/reception/discount/detail');
const Expanses = () => import('@/components/reception/expanses/expanses');
const ReceptionPatients = () => import('@/components/reception/patients/patients');
const ReceptionPatientSearch = () => import('@/components/reception/patients/search/search');

// Doctor
const Doctor = () => import('@/components/doctor/Doctor');
const DoctorDash = () => import('@/components/doctor/dash');
const DoctorPatients = () => import('@/components/doctor/patients/patients');
const DoctorPatientDetail = () => import('@/components/doctor/patients/detail');

// Assistant
const Assistant = () => import('@/components/assistant/Assistant');
const AssistantPatients = () => import('@/components/assistant/patients/patients');
const AssistantPatientDetail = () => import('@/components/assistant/patients/detail');

// Admin
const Admin = () => import('@/components/admin/Admin');
const AdminDash = () => import('@/components/admin/dashboard/dashboard');

// Admin Users
const AdminUsers = () => import('@/components/admin/users/users');
const AdminUsersList = () => import('@/components/admin/users/list');
const AdminUsersNew = () => import('@/components/admin/users/new');
const AdminUsersEdit = () => import('@/components/admin/users/edit');

// Admin Departments
const AdminDepartments = () => import('@/components/admin/departments/departments');
const AdminDepartmentsList = () => import('@/components/admin/departments/list');
const AdminDepartmentsNew = () => import('@/components/admin/departments/new');
const AdminDepartmentsEdit = () => import('@/components/admin/departments/edit');

// Admin Doctors
const AdminDoctors = () => import('@/components/admin/doctors/doctors');
const AdminDoctorsList = () => import('@/components/admin/doctors/list');
const AdminDoctorsNew = () => import('@/components/admin/doctors/new');
const AdminDoctorsEdit = () => import('@/components/admin/doctors/edit');

// Admin Employees
const AdminEmployees = () => import('@/components/admin/employees/employees');
const AdminEmployeesList = () => import('@/components/admin/employees/list');
const AdminEmployeesNew = () => import('@/components/admin/employees/new');
const AdminEmployeesEdit = () => import('@/components/admin/employees/edit');

// Admin Income
const AdminIncomes = () => import('@/components/admin/incomes/incomes');
const AdminAccount = () => import('@/components/admin/incomes/account/income');
const AdminDeposit = () => import('@/components/admin/incomes/deposits/deposit');
const AdminExpanse = () => import('@/components/admin/incomes/expanse/expanse');
const AdminExpanseAdd = () => import('@/components/admin/incomes/expanse/addExpanse');

// Admin Shares
const AdminShares = () => import('@/components/admin/shares/shares');
const AdminDepartmentShares = () => import('@/components/admin/shares/department/department');
const AdminDoctorShares = () => import('@/components/admin/shares/doctor/doctor');
const AdminEmployeeShares = () => import('@/components/admin/shares/employee/employee');

// Admin Patients
const AdminPatients = () => import('@/components/admin/patients/patients');
const AdminSearch = () => import('@/components/admin/patients/search/search');

Vue.use(Router);

export default new Router({
  linkActiveClass: 'active',
  routes: [
    /******************************
     * Login
     *****************************/
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    /******************************
     * Reception
     *****************************/
    {
      path: '/reception',
      component: Reception,
      children: [
        {
          path: '',
          name: 'Reception.OutDoor',
          component: OutDoor
        },
        {
          path: 'discount',
          name: 'Reception.Discount',
          component: Discount,
          children: [
            { path: 'detail/:id', component: DiscountDetail, props: true }
          ]
        },
        {
          path: 'expanses',
          name: 'Reception.Expanses',
          component: Expanses
        },
        {
          path: 'patients',
          component: ReceptionPatients,
          children: [
            { path: '', name: 'ReceptionPatients.Search', component: ReceptionPatientSearch }
          ]
        }
      ]
    },
    /******************************
     * Doctor
     *****************************/
    {
      path: '/doctor',
      name: 'Doctor',
      component: Doctor,
      children: [
        {
          path: '',
          name: 'Doctor.Dash',
          component: DoctorDash
        },
        {
          path: 'patients',
          name: 'Doctor.Patients',
          component: DoctorPatients,
          children: [
            { path: 'detail/:id', component: DoctorPatientDetail, props: true }
          ]
        }
      ]
    },
    /******************************
     * Assistant
     *****************************/
    {
      path: '/assistant',
      name: 'Assistant',
      component: Assistant,
      children: [
        {
          path: 'patients',
          name: 'Assistant.Patients',
          component: AssistantPatients,
          children: [
            { path: 'detail/:id', component: AssistantPatientDetail, props: true }
          ]
        }
      ]
    },
    /******************************
     * Admin
     *****************************/
    {
      path: '/admin',
      component: Admin,
      children: [
        {
          path: '',
          name: 'Admin.Dash',
          component: AdminDash
        },
        // Users
        {
          path: 'users',
          component: AdminUsers,
          children: [
            { path: '', name: 'AdminUsers.Lists', component: AdminUsersList },
            { path: 'new', name: 'AdminUsers.New', component: AdminUsersNew },
            { path: 'edit/:id', name: 'AdminUsers.Edit', component: AdminUsersEdit, props: true }
          ]
        },
        // Departments
        {
          path: 'departments',
          component: AdminDepartments,
          children: [
            { path: '', name: 'AdminDepartments.Lists', component: AdminDepartmentsList },
            { path: 'new', name: 'AdminDepartments.New', component: AdminDepartmentsNew },
            { path: 'edit/:editId', name: 'AdminDepartments.Edit', component: AdminDepartmentsEdit, props: true }
          ]
        },
        // Doctors
        {
          path: 'doctors',
          component: AdminDoctors,
          children: [
            { path: '', name: 'AdminDoctors.Lists', component: AdminDoctorsList },
            { path: 'new', name: 'AdminDoctors.New', component: AdminDoctorsNew },
            { path: 'edit/:id', name: 'AdminDoctors.Edit', component: AdminDoctorsEdit, props: true }
          ]
        },
        // Employees
        {
          path: 'employees',
          component: AdminEmployees,
          children: [
            { path: '', name: 'AdminEmployees.Lists', component: AdminEmployeesList },
            { path: 'new', name: 'AdminEmployees.New', component: AdminEmployeesNew },
            { path: 'edit/:id', name: 'AdminEmployees.Edit', component: AdminEmployeesEdit, props: true }
          ]
        },
        // Income
        {
          path: 'incomes',
          component: AdminIncomes,
          children: [
            { path: '', name: 'AdminIncomes.Income', component: AdminAccount },
            { path: 'deposits', name: 'AdminIncomes.Deposits', component: AdminDeposit },
            { path: 'expanses', name: 'AdminIncomes.Expanse', component: AdminExpanse },
            { path: 'expanses/add', name: 'AdminIncomes.AddExpanse', component: AdminExpanseAdd }
          ]
        },
        // Shares
        {
          path: 'shares',
          component: AdminShares,
          children: [
            { path: 'department', name: 'AdminShares.Department', component: AdminDepartmentShares },
            { path: 'doctor', name: 'AdminShares.Doctor', component: AdminDoctorShares },
            { path: 'employee', name: 'AdminShares.Employee', component: AdminEmployeeShares }
          ]
        },
        // patient
        {
          path: 'patients',
          component: AdminPatients,
          children: [
            { path: '', name: 'AdminPatients.Search', component: AdminSearch }
          ]
        }
      ]
    }
  ]
});
