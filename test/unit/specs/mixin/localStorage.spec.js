import Storage from '@/resource/localStorage';

describe('Local Storage', () => {
  let store = new Storage();
  const localStorageMock = {
    getItem: jest.fn(),
    setItem: jest.fn(),
    clear: jest.fn()
  };
  global.sessionStorage = localStorageMock;
  it('should set storage to session on creation', () => {
    expect(store.storage).toEqual('session');
  });
  it('should store value', async () => {
    await store.setValue({'key': 'value'});
    await expect(store.getValue('key')).toBeInstanceOf(Object);
  });
});
