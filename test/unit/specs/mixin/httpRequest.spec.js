// Import Vue and the component being tested
import { createLocalVue, shallow } from 'vue-test-utils';
import httpRequest from '@/mixin/httpRequest';
import Mixin from '@/mixin/mixin';
import axios from 'axios';

describe('HttpRequest', () => {
  const localVue = createLocalVue();
  localVue.mixin(Mixin);
  const $router = {
    path: '/some/path',
    name: 'name',
    push: function (p) {
      if (p.path) {
        this.path = p.path;
      }
      if (p.name) {
        this.name = p.name
      }
    }
  };
  const wrapper = shallow(httpRequest, {
    localVue,
    mocks: {
      $router
    }
  });
  const vm = wrapper.vm;
  const parseResponseHolder = vm.parseResponse;

  // test function calls
  describe('Functions Calls', () => {
    const callRemoteGet = jest.spyOn(vm, 'callRemoteGet');
    const callRemotePost = jest.spyOn(vm, 'callRemotePost');
    const callRemotePut = jest.spyOn(vm, 'callRemotePut');
    const params = {foo: 'bar'};
    afterAll(() => {
      jest.restoreAllMocks();
    });
    it('should throw on wrong parameters', async () => {
      await expect(vm.makeRequest()).rejects.toBeInstanceOf(Error);
    });
    it('should set target data ', async () => {
      await vm.makeRequest('GET', 'url', params);
      expect(vm.requestTarget).toBe('url');
      expect(vm.requestParams).toMatchObject(params);
    });
    it('should call get/post/put caller methods accordingly to params', async() => {
      // test Get
      await vm.makeRequest('GET', 'url', {});
      expect(vm.callRemoteGet).toBeCalled();
      // test POST
      await vm.makeRequest('POST', 'url', {});
      expect(vm.callRemotePost).toBeCalled();
      // test PUT
      await vm.makeRequest('PUT', 'url', {});
      expect(vm.callRemotePut).toBeCalled();
    });
  });
  // test remote api calls
  describe('Remote Api', () => {
    const params = {foo: 'bar'};
    const url = 'user/auth';
    beforeEach(() => {
      jest.clearAllMocks();
      wrapper.setData({
        requestParams: params,
        requestTarget: url
      });
    });
    afterAll(() => {
      vm.parseResponse = parseResponseHolder;
    });
    const localStorageMock = {
      getItem: jest.fn(),
      setItem: jest.fn(),
      clear: jest.fn()
    };
    global.sessionStorage = localStorageMock;
    vm.parseResponse = jest.fn();
    it('should call remote get api', async() => {
      await vm.callRemoteGet();
      expect(axios.get).toHaveBeenCalledWith(url, {
        headers: {},
        params: params
      });
      expect(vm.parseResponse).toHaveBeenCalledTimes(1);
    });
    it('should call remote post api', async() => {
      await vm.callRemotePost();
      expect(axios.post).toHaveBeenCalledWith(url, params, {
        headers: {}
      });
      expect(vm.parseResponse).toHaveBeenCalledTimes(1);
    });
    it('should call remote put api', async() => {
      await vm.callRemotePut();
      expect(axios.put).toHaveBeenCalledWith(url, params, {
        headers: {}
      });
      expect(vm.parseResponse).toHaveBeenCalledTimes(1);
    });
  });
  // test server response
  describe('Parse Server Response', () => {
    // throw error on empty response
    it('should throw error on empty server response', async () => {
      await expect(vm.parseResponse()).rejects.toBeInstanceOf(Error);
    });
    // process response
    describe('Process Response', () => {
      beforeEach(() => {
        jest.clearAllMocks();
      });
      let returnData = {data: 'data'};
      let response = {
        headers: {'x-auth': 'token'},
        status: 200,
        data: {
          status: true,
          data: returnData
        }
      };
      // test 200 response
      it('should return response data', async () => {
        let result = await vm.parseResponse(response);
        expect(result).toMatchObject(returnData);
      });
      // test server status false response
      it('should throw error on false status', async () => {
        response.data.status = false;
        await expect(vm.parseResponse(response)).rejects.toBeInstanceOf(Error);
      });
      // test server status false response
      it('should throw error when response is not 200', async () => {
        response.data.status = true;
        response.status = 300;
        await expect(vm.parseResponse(response)).rejects.toBeInstanceOf(Error);
      });
      it('should redirect when response has redirect', async () => {
        response.data.status = true;
        response.status = 200;
        response.data.data.redirect = '/some/other/url';
        await vm.parseResponse(response);
        expect(vm.$router.path).toEqual('/some/other/url');
      });
    });
  });
  // test server generated errors
  describe('Parse Errors', () => {
    vm.$message = jest.fn();
    const error = {
      message: 'msg',
      response: {
        status: 401
      }
    };
    it('should redirect to Login on 401 error', () => {
      vm.parseError(error);
      expect(vm.$message).toBeCalled();
      expect(vm.$router.name).toEqual('Login');
    });
    it('should show error on any other error', () => {
      error.response.status = 400;
      vm.parseError(error);
      expect(vm.$message).toBeCalled();
    });
  });
});
