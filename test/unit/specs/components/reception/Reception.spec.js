jest.mock('http', () => ({
  makeRequest: jest.fn()
}));
// Import Vue and the component being tested
import { createLocalVue, shallow } from 'vue-test-utils';
import MyComponent from '@/components/reception/Reception.vue';
import api from '@/resource/apiEndpoints';
import ElementUI from 'element-ui';
import http from 'http';
import Mixin from '@/mixin/mixin';
import VueRouter from 'vue-router';

describe('Reception', () => {
  const localVue = createLocalVue();
  localVue.use(ElementUI);
  localVue.mixin(Mixin);
  const wrapper = shallow(MyComponent, {
    localVue,
    mocks: {
      api
    }
  });
  it('should be rendered', () => {
    expect(wrapper.contains('.top-menu')).toBe(true);
  });
});
