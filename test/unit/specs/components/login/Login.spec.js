jest.mock('http', () => ({
  makeRequest: jest.fn()
}));
// Import Vue and the component being tested
import { createLocalVue, mount } from 'vue-test-utils';
import MyComponent from '@/components/login/Login.vue';
import api from '@/resource/apiEndpoints';
import ElementUI from 'element-ui';
import http from 'http';
import Mixin from '@/mixin/mixin';

const params = {
  form: {
    login_id: 'bar',
    password: 'foo'
  }
};

describe('Login', () => {
  const localVue = createLocalVue();
  localVue.use(ElementUI);
  localVue.mixin(Mixin);
  const wrapper = mount(MyComponent, {
    localVue,
    mocks: {
      api
    }
  });
  const vm = wrapper.vm;
  const button = wrapper.find('button');

  beforeEach(() => {
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('has rendered', () => {
    expect(wrapper.contains('form')).toBe(true);
  });
  it('should fail on empty field(s)', () => {
    button.trigger('click');
    expect(vm.formSubmitting).toBe(false);
  });
  describe('Login Functionality', () => {
    beforeEach(() => {
      wrapper.setData(params);
    });
    it('should call submit method', () => {
      const makeRequestStub = jest.fn();
      wrapper.setMethods({submit: makeRequestStub});
      button.trigger('click');
      expect(makeRequestStub).toBeCalled();
    });
  });
});
