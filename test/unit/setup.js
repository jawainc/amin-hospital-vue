import Vue from 'vue';

Vue.config.productionTip = false;
Vue.config.ignoredElements = ['iron-icon'];
