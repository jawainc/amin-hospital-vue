const axios = jest.genMockFromModule('axios');

axios.get = jest.fn(() => Promise.resolve({ data: [3] }))
axios.post = jest.fn();
axios.put = jest.fn();

module.exports = axios;
